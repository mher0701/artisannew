<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('admin', 'AdminController@getIndex');

Route::get('login', 'AuthController@login');
Route::post('login', 'AuthController@processLogin');
Route::get('signup', 'AuthController@signup');
Route::post('signup', 'AuthController@processSignup');
Route::get('signup-closed', 'AuthController@signupClosed');
Route::get('signup/invite/{token}', 'AuthController@inviteSignup');
Route::post('signup/invite/{token}', 'AuthController@processInviteSignup');
Route::get('logout', 'AuthController@logout');

// Route::controller('password', 'RemindersController');
Route::get('password/remind', 'RemindersController@getRemind');
Route::post('password/remind', 'RemindersController@postRemind');
Route::get('password/reset/{token?}', 'RemindersController@getReset');
Route::post('password/reset', 'RemindersController@postReset');


Route::get('/', 'SiteController@index');
Route::get('/pricing', 'SiteController@pricing');
Route::get('/terms', 'SiteController@terms');
Route::get('/privacy', 'SiteController@privacy');



Route::get('account', 'AccountController@getIndex');
Route::post('account/info', 'AccountController@postInfo'); 
Route::post('account/password', 'AccountController@postPassword');

Route::get('team/{project_id?}', 'TeamController@index');
Route::post('team/add-user', 'TeamController@addUser');
Route::post('team/remove-user', 'TeamController@removeUser');
Route::get('import/basecamp', 'ImportController@basecamp');
Route::get('import/basecamp/login', 'ImportController@basecampLogin');
Route::post('import/basecamp/go', 'ImportController@doImport');


Route::get('app', 'AccountController@getAppPage');


Route::group(array('prefix' => 'api/1'), function()
{
	Route::get('/', function(){
		return response()->json(array(
			'error' => array(
				'code' => 404,
				'message' => 'Not found'
			)
		), 404);
	});
    Route::resource('projects', 'api\ProjectsController');
	Route::resource('milestones', 'api\MilestonesController');
	Route::resource('tasks', 'api\TasksController');
	Route::resource('task-comments', 'api\TaskCommentsController');
	Route::resource('task-comment-attachments', 'api\TaskCommentAttachmentsController');
	Route::resource('task-categories', 'api\TaskCategoriesController');
	Route::resource('task-phases', 'api\TaskPhasesController');
	Route::resource('users', 'api\UsersController');
	Route::resource('history', 'api\HistoryController');
});

Route::get('billing', 'BillingController@index');
Route::get('billing/upgrade/{plan}', 'BillingController@upgrade');
Route::post('billing/upgrade/{plan}', 'BillingController@processUpgrade');
Route::get('billing/edit', 'BillingController@edit');
//Route::post('billing/edit', 'BillingController@processEdit');
Route::post('billing/cancel', 'BillingController@processCancel');
Route::get('billing/invoice/{id}', 'BillingController@invoice');