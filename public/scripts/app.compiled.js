// Global App
window.App = {
	Config: {
		apiRoot: '/api/1'
	},
	Models: {},
	Collections: {},
	Views: {},
	Router: {},
	Sync: {},
	Templates: {},
	Global: {}
};

/*
* Store a version of Backbone.sync to call from the
* modified version we create
*/
App.Sync.backboneSync = Backbone.sync;
Backbone.sync = function(method, model, options) {

	options.headers = {
		'X-API-KEY': currentUser.apiKey
	};

	/*
	* Call the stored original Backbone.sync method with
	* extra headers argument added
	*/
	return App.Sync.backboneSync(method, model, options);

};

/*
 * Router
 */
var AppRouter = Backbone.Router.extend({

	routes: {
		'': 'index',
		'projects': 'projects',
		'project/:id': 'project',
		'milestone/:id': 'milestone',
		'task/:id': 'task',
		'activity': 'activity'
	}

});
App.Router = new AppRouter();

/*
 * Global events pub/sub
 */
App.Global.Events = _.extend({}, Backbone.Events);

/*
 * Backbone Form templates
 */
if(typeof Backbone.Form !== 'undefined'){
	Backbone.Form.template = _.template('<form class="backbone-form"><div data-fieldsets></div><input type="button" value="Cancel" class="btn alt cancel"> <input type="button" value="Save" class="btn submit"></form>');
	var defaultFormTemplate = _.template('<form data-fieldsets></form>');
	var selectFieldTemplate = _.template('<div class="styled-select" data-editor></div>');
}

/*
 * Underscore template helpers
 */
function gravatar(email, size){
	var s = (typeof size !== "undefined") ? size : 40;
	return "https://www.gravatar.com/avatar/" + md5( email ) + "?s="+ s +"&d=mm";
}

function formatSize(size) {
	if (typeof size === 'undefined' || /\D/.test(size)) {
		return 'N/A';
	}
	function round(num, precision) {
		return Math.round(num * Math.pow(10, precision)) / Math.pow(10, precision);
	}
	var boundary = Math.pow(1024, 4);
	// TB
	if (size > boundary) {
		return round(size / boundary, 1) + ' tb';
	}
	// GB
	if (size > (boundary/=1024)) {
		return round(size / boundary, 1) + ' gb';
	}
	// MB
	if (size > (boundary/=1024)) {
		return round(size / boundary, 1) + ' mb';
	}
	// KB
	if (size > 1024) {
		return Math.round(size / 1024) + ' kb';
	}
	return size + ' b';
}

function stripHTML(html)
{
	var tmp = document.createElement("DIV");
	tmp.innerHTML = html;
	return tmp.textContent || tmp.innerText || "";
}

/*
 * Handlebars template helpers
 */
if(typeof Handlebars !== 'undefined'){
	// Handlebars.js - Gravatar thumbnail
	// Usage: {{#gravatar email size="64"}}{{/gravatar}} [depends on md5.js]
	// Author: Makis Tracend (@tracend)
	Handlebars.registerHelper('gravatar', function(context, options) {
		var email = context;
		var size=( typeof(options.hash.size) === "undefined") ? 32 : options.hash.size;
		return "https://www.gravatar.com/avatar/" + md5( email ) + "?s="+ size +"&d=mm";
	});

	Handlebars.registerHelper('formatDate', function(datetime, options) {
		if(datetime == '' || datetime == '0000-00-00 00:00:00') return '';
		var dateFormats = {
			'default': 'D MMM YYYY',
			'relative': ''
		};
		if(moment){
			if(!options.hash.format) options.hash.format = 'default';
			if(options.hash.format == 'relative'){
				return moment(datetime).fromNow();
			} else {
				f = dateFormats[options.hash.format];
				return moment(datetime).format(f);
			}
		}
		return datetime;
	});
}

this["App"] = this["App"] || {};
this["App"]["Templates"] = this["App"]["Templates"] || {};

this["App"]["Templates"]["ActivityItem"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


if(action == 'deleted'){
	print('<a href="#/activity">');
} else {
	if(type == 'comment'){
		print('<a href="#/task/'+ content.task_id +'">');
	} else {
		print('<a href="#/'+ type +'/'+ type_id +'">');
	}
}
;
__p += '\n<span class="user">' +
((__t = ( user ? user.name : 'Unknown' )) == null ? '' : __t) +
'</span>\n<span class="action">';

if(action == 'created' && type == 'comment'){
	print('commented')
} else {
	print(action +' the '+ type);
}
;
__p += '</span>\n';

if(type == 'comment'){
	print('<span class="object">&ldquo;'+ stripHTML(content.content.substring(0,10)) +'...&rdquo;</span>');
} else {
	print('<span class="object">'+ content.name +'</span>');
}
;
__p += '\n</a>\n';

}
return __p
};

this["App"]["Templates"]["HistoryItem"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<img src="' +
((__t = ( gravatar((user ? user.email : 'hi@artisan.so'), 30) )) == null ? '' : __t) +
'" alt="">\n<span class="user">' +
((__t = ( user ? user.name : 'Unknown' )) == null ? '' : __t) +
'</span>\n<span class="action">';

if(action == 'created' && type == 'comment'){
	print('commented')
} else {
	print(action +' the '+ type);
}
;
__p += '</span>\n';

if(type == 'comment'){
	if(action == 'deleted'){
		print('<span class="object">&ldquo;'+ stripHTML(content.content.substring(0,50)) +'...&rdquo;</span>');
	} else {
		print('&ldquo;<a href="#/task/'+ content.task_id +'" class="object">'+ stripHTML(content.content.substring(0,50)) +'...</a>&rdquo;');
	}
} else {
	if(action == 'deleted'){
		print('<span class="object">'+ content.name +'</span>');
	} else {
		print('<a href="#/'+ type +'/'+ type_id +'" class="object">'+ content.name +'</a>');
	}
}
;
__p += '\n<span class="time">' +
((__t = ( moment(created_at).fromNow() )) == null ? '' : __t) +
'</span>\n';

}
return __p
};

this["App"]["Templates"]["TaskCommentAttachmentCreate"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p +=
((__t = ( name )) == null ? '' : __t) +
' <span class="size">(' +
((__t = ( formatSize(size) )) == null ? '' : __t) +
')</span>\n<span class="progress"></span>\n<a href="#" class="icon-cross delete-attachment" title="Delete ' +
((__t = ( name )) == null ? '' : __t) +
'"></a>\n';

}
return __p
};

this["App"]["Templates"]["TaskCommentAttachment"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<a href="//cdn.artisan.so/' +
((__t = ( filename )) == null ? '' : __t) +
'" target="_blank" title="Download ' +
((__t = ( name )) == null ? '' : __t) +
'" download="' +
((__t = ( name )) == null ? '' : __t) +
'">\n\t<div class="thumb' +
((__t = ( file_type != 'image' ? ' icon-file' : '' )) == null ? '' : __t) +
' ' +
((__t = ( extension )) == null ? '' : __t) +
'"' +
((__t = ( file_type == 'image' ? ' style="background-image:url(//cdn.artisan.so/'+ filename +')"' : '' )) == null ? '' : __t) +
'></div>\n\t<div class="name">' +
((__t = ( name )) == null ? '' : __t) +
'</div>\n\t<div class="meta">\n\t\t<span class="size">' +
((__t = ( formatSize(size) )) == null ? '' : __t) +
'</span>\n\t</div>\n</a>\n';

}
return __p
};
this["App"] = this["App"] || {};
this["App"]["Templates"] = this["App"]["Templates"] || {};

this["App"]["Templates"]["ActivityMenu"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<a href=\"#\" class=\"icon-bell\" title=\"Activity\"></a>\n<ul class=\"dropit-submenu\">\n	<li class=\"loading\"><a href=\"#/activity\">Loading...</a></li>\n	<li class=\"view-activity\"><a href=\"#/activity\">View Recent Activity</a></li>\n</ul>\n";
  });

this["App"]["Templates"]["FormattingHelp"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Formatting Help</h3>\n</div>\n<div class=\"modal-content\">\n	<p>Artisan supports Markdown editing in comments. Here are some examples:</p>\n	<p><strong>Italics &amp; Bold</strong></p>\n	<table>\n		<tr>\n			<td style=\"width:50%\"><code>*This is italicized*</code></td>\n			<td><em>This is italicized</em></td>\n		</tr>\n		<tr>\n			<td><code>**This is bold**</code></td>\n			<td><strong>This is bold</strong></td>\n		</tr>\n	</table>\n	<p><strong>Links</strong></p>\n	<table>\n		<tr>\n			<td style=\"width:50%\"><code>[artisan](http://artisan.so)</code></td>\n			<td><a href=\"http://artisan.so\" target=\"_blank\">artisan</a></td>\n		</tr>\n	</table>\n	<p><strong>Lists</strong></p>\n	<table>\n		<tr>\n			<td style=\"width:50%\"><pre><code>* List item 1\n* List item 2\n* List item 3</code></pre></td>\n			<td><ul><li>List item 1</li><li>List item 2</li><li>List item 3</li></ul></td>\n		</tr>\n		<tr>\n			<td><pre><code>1. List item 1\n2. List item 2\n3. List item 3</code></pre></td>\n			<td><ol><li>List item 1</li><li>List item 2</li><li>List item 3</li></ol></td>\n		</tr>\n	</table>\n</div>\n<div class=\"modal-footer\">\n	<a href=\"#\" class=\"btn alt cancel\">Close</a>\n</div>\n";
  });

this["App"]["Templates"]["FreeTrialModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Start your <del>14</del> 60 day free trial</h3>\n</div>\n<div class=\"modal-content\">\n	<p>Once you start your free trial you will be able to create your own projects and\n	invite members to your team. At the end of the free trial you will need to upgrade to\n	a paid plan to continue using these features.</p>\n</div>\n<div class=\"modal-footer\">\n	<a href=\"#\" class=\"btn alt cancel\">Cancel</a>\n	<a href=\"/start-free-trial\" class=\"btn start\">Start Free Trial</a>\n</div>\n";
  });

this["App"]["Templates"]["MilestoneCreate"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"create-milestone-wrap\">\n	<a href=\"#\" class=\"create-milestone\">Create Milestone +</a>\n</div>\n<div class=\"milestone-form\"></div>\n";
  });

this["App"]["Templates"]["MilestoneEditModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Edit Milestone</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["MilestoneList"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"milestone-header\">\n	<a href=\"#/milestone/";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">\n		<span class=\"milestone-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n		<span class=\"milestone-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	</a>\n</div>\n<div class=\"milestone-body\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["Milestone"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<div class=\"milestone-header\">\n	<span class=\"breadcrumb\">\n		<a href=\"#/project/"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.project)),stack1 == null || stack1 === false ? stack1 : stack1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"project-breadcrumb\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.project)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n		<span class=\"sep\">/</span>\n	</span>\n	<span class=\"milestone-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"milestone-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<div class=\"milestone-meta\">\n		<span class=\"milestone-createdat\">Created on "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.created_at), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.created_at), options)))
    + "</span>\n		<a href=\"#\" class=\"icon-pencil milestone-edit\" title=\"Edit Milestone\"></a>\n		<a href=\"#\" class=\"icon-trash milestone-delete\" title=\"Delete Milestone\"></a>\n	</div>\n</div>\n<div class=\"milestone-body\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["NotFound"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<h1>Whoops!</h1>\n<p>It looks like this doesn't exist or has been deleted. Sorry about that.</p>\n";
  });

this["App"]["Templates"]["ProjectCreateModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Create Project</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["ProjectEditModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Edit Project</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["ProjectList"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, self=this, helperMissing=helpers.helperMissing, functionType="function", escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n				<li><img src=\"";
  stack1 = (helper = helpers.gravatar || (depth0 && depth0.gravatar),options={hash:{
    'size': ("30")
  },inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.email), options) : helperMissing.call(depth0, "gravatar", (depth0 && depth0.email), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" title=\""
    + escapeExpression(((stack1 = (depth0 && depth0.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" alt=\"\"></li>\n			";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  buffer += "<a href=\"#/project/";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" class=\"project-link\">\n	<span class=\"project-name\" title=\"";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"project-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"project-meta\">\n		<ul class=\"users\">\n			";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.users), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		</ul>\n	</span>\n</a>\n<a href=\"#\" class=\"icon-pencil project-edit\" title=\"Edit Project\"></a>\n<a href=\"#\" class=\"icon-trash project-delete\" title=\"Delete Project\"></a>\n";
  return buffer;
  });

this["App"]["Templates"]["ProjectSelect"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += "\n		<option value=\"";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</option>\n		";
  return buffer;
  }

  buffer += "<div class=\"styled-select\">\n	<select>\n		<option value=\"all\">All Projects</option>\n		";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.projects), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	</select>\n</div>\n";
  return buffer;
  });

this["App"]["Templates"]["Projects"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<h3 class=\"title your-projects-title\">Your Projects</h3>\n<ul class=\"your-projects\"></ul>\n\n<h3 class=\"title shared-projects-title\">Shared Projects</h3>\n<ul class=\"shared-projects\"></ul>\n";
  });

this["App"]["Templates"]["TaskCommentCreate"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  buffer += "<div class=\"comment-author\">\n	<span class=\"avatar\"><img src=\"";
  stack1 = (helper = helpers.gravatar || (depth0 && depth0.gravatar),options={hash:{
    'size': ("40")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options) : helperMissing.call(depth0, "gravatar", ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" alt=\"\"></span>\n</div>\n<div class=\"comment-form\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["TaskComment"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

function program3(depth0,data) {
  
  var stack1;
  return escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1));
  }

function program5(depth0,data) {
  
  
  return "Unknown";
  }

  buffer += "<div class=\"comment-author\">\n	<span class=\"avatar\"><img src=\"";
  stack1 = (helper = helpers.gravatar || (depth0 && depth0.gravatar),options={hash:{
    'size': ("40")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options) : helperMissing.call(depth0, "gravatar", ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" alt=\"\"></span>\n	<span class=\"name\">";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.name), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span><br>\n	<span class=\"date\">Posted "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{
    'format': ("relative")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.created_at), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.created_at), options)))
    + "</span>\n	<span class=\"comment-delete\">- <a href=\"#\" class=\"delete\">Delete</a></span>\n</div>\n<div class=\"comment-content\">";
  if (helper = helpers.content) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.content); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</div>\n<div class=\"comment-attachments\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["TaskEditModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Edit Task</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["TaskList"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<span class=\"task-category\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.category)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n	";
  return buffer;
  }

function program3(depth0,data) {
  
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.phase), {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program4(depth0,data) {
  
  
  return "\n	<span class=\"sep\">/</span>\n	";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<span class=\"task-phase\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.phase)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n	";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<span class=\"task-assignedto\">("
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.assigned_user)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ")</span>\n	";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\n	<span class=\"task-duedate\">"
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options)))
    + "</span>\n	";
  return buffer;
  }

function program12(depth0,data) {
  
  
  return " checked=\"checked\"";
  }

  buffer += "<div class=\"pull-right\">\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.category), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.category), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.phase), {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.assigned_user), {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = (helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options));
  stack1 = helpers['if'].call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>\n<input type=\"checkbox\" class=\"completed\"";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.completed), {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n<a href=\"#/task/";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">\n	<span class=\"task-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"task-description\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n</a>\n";
  return buffer;
  });

this["App"]["Templates"]["Task"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<span class=\"task-category\"><strong>Category:</strong> "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.category)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n		";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<span class=\"task-phase\"><strong>Phase:</strong> "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.phase)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n		";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<span class=\"task-assignedto\"><strong>Assigned to:</strong> "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.assigned_user)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n		";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\n		<span class=\"task-duedate\">Due date: "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options)))
    + "</span>\n		";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n		<span class=\"task-createdby\">Created by "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.created_by)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{
    'format': ("relative")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.created_at), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.created_at), options)))
    + "</span>\n		";
  return buffer;
  }

  buffer += "<div class=\"task-header\">\n	<span class=\"breadcrumb\">\n		<a href=\"#/project/"
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.milestone)),stack1 == null || stack1 === false ? stack1 : stack1.project)),stack1 == null || stack1 === false ? stack1 : stack1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"project-breadcrumb\">"
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.milestone)),stack1 == null || stack1 === false ? stack1 : stack1.project)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n		<span class=\"sep\">/</span>\n		<a href=\"#/milestone/";
  if (helper = helpers.milestone_id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.milestone_id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" class=\"milestone-breadcrumb\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.milestone)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n		<span class=\"sep\">/</span>\n	</span>\n	<span class=\"task-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"task-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<div class=\"task-meta\">\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.category), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.phase), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.assigned_user), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = (helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options));
  stack1 = helpers['if'].call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.created_by), {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		<a href=\"#\" class=\"icon-tick task-complete\" title=\"Complete Task\"></a>\n		<a href=\"#\" class=\"icon-pencil task-edit\" title=\"Edit Task\"></a>\n		<a href=\"#\" class=\"icon-trash task-delete\" title=\"Delete Task\"></a>\n	</div>\n</div>\n<div class=\"task-body\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["UpgradeModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Time to Upgrade</h3>\n</div>\n<div class=\"modal-content\"></div>\n<div class=\"modal-footer\">\n	<a href=\"#\" class=\"btn alt cancel\">Cancel</a>\n	<a href=\"/billing\" class=\"btn upgrade\">Upgrade</a>\n</div>\n";
  });
App.Models.History = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/history'

});

App.Models.Milestone = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/milestones'

});

App.Models.Project = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/projects'

});

App.Models.Task = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/tasks'

});

App.Models.TaskComment = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-comments'

});

App.Models.TaskCommentAttachment = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-comment-attachments'

});

App.Models.TaskCategory = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-categories',

	schema: {
		id: { type: 'Hidden' },
		name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Category name' } },
		description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
	},

	toString: function() {
        return this.get('name');
    }

});

App.Models.TaskPhase = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-phases',

	schema: {
		id: { type: 'Hidden' },
		name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Phase name' } },
		description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
	},

	toString: function() {
		return this.get('name');
	}

});

App.Models.User = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/users'

});

App.Collections.History = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/history',
	model: App.Models.History

});

App.Collections.Milestones = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/milestones',
	model: App.Models.Milestone,
	comparator: function(model) {
		return model.get('sort');
	}

});

App.Collections.Projects = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/projects',
	model: App.Models.Project,
	comparator: function(model) {
		return model.get('sort');
	}

});

App.Collections.Tasks = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/tasks',
	model: App.Models.Task,
	comparator: function(model) {
        return model.get('sort');
    }

});

App.Collections.TaskComments = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/task-comments',
	model: App.Models.TaskComment

});

App.Collections.TaskCommentAttachments = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/task-comment-attachments',
	model: App.Models.TaskCommentAttachment

});

App.Collections.TaskCategories = Backbone.Model.extend({

	url: App.Config.apiRoot +'/task-categories',
	model: App.Models.TaskComment

});

App.Collections.TaskPhases = Backbone.Model.extend({

	url: App.Config.apiRoot +'/task-phases',
	model: App.Models.TaskComment

});

App.Collections.Users = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/users',
	model: App.Models.User

});

App.Views.ActivityMenuView = Backbone.View.extend({

	tagName: 'li',
	className: 'activity dropit-trigger',
	template: App.Templates.ActivityMenu,

	events: {
		'click .icon-bell': 'openMenu'
	},

	initialize: function() {
		this.history = new App.Collections.History({ model: App.Models.History });

		_.bindAll(this, 'loaded');
		this.listenTo(this.history, 'add', this.addOne);
		this.listenTo(this.history, 'reset', this.addAll);

		$('.settings-nav .settings').before(this.render().el);
	},

	openMenu: function() {
		this.history.fetch({
			success: this.loaded,
			data: { limit: 5 }
		});
	},

	loaded: function() {
		if(this.history.length){
			this.$el.find('.loading').remove();
			this.$el.find('.view-activity').show();
		} else {
			this.$el.find('.loading a').text('No recent activity');
			this.$el.find('.view-activity').hide();
		}
	},

	addOne: function(model) {
		var view = new App.Views.ActivityItemView({ model: model });
		view.render().$el.insertBefore(this.$el.find('.view-activity'));
	},

	addAll: function() {
		this.$el.find('.item').remove();
		this.history.each(this.addOne, this);
	}

});

App.Views.ActivityItemView = Backbone.View.extend({

	tagName: 'li',
	className: 'item',
	template: App.Templates.ActivityItem,

	initialize: function() {
		this.model.set('content', JSON.parse(this.model.get('content')));

		this.listenTo(this.model, 'sync', this.render);
	},

	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}

});

App.Views.HistoryView = Backbone.View.extend({

	tagName: 'ul',
	id: 'activity',

	initialize: function(atts) {
		this.history = new App.Collections.History({ model: App.Models.History });
		this.$el.appendTo('#content');

		this.listenTo(this.history, 'add', this.addOne);
		this.listenTo(this.history, 'reset', this.addAll);

		this.history.fetch();
	},

	addOne: function(model) {
		var view = new App.Views.HistoryItemView({ model: model });
		this.$el.append(view.render().el);
	},

	addAll: function() {
		this.$el.html('');
		this.history.each(this.addOne, this);
	}

});

App.Views.HistoryItemView = Backbone.View.extend({

	tagName: 'li',
	className: 'item',
	template: App.Templates.HistoryItem,

	initialize: function() {
		this.model.set('content', JSON.parse(this.model.get('content')));

		this.listenTo(this.model, 'sync', this.render);
	},

	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}

});

App.Views.MilestoneListView = Backbone.View.extend({

	tagName: 'li',
	className: 'milestone',
	template: App.Templates.MilestoneList,

	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		this.$el.attr('data-id', this.model.get('id')); // For sorting
		var view = new App.Views.MilestoneTasksView({ model: this.model });
		this.$el.find('.milestone-body').append(view.render().el);
		return this;
	}

});

App.Views.CreateMilestoneView = Backbone.View.extend({

	tagName: 'li',
	className: 'milestone-create cf',
	template: App.Templates.MilestoneCreate,

	events: {
		'click .create-milestone': 'showForm',
		'click .cancel': 'hideForm',
		'click .submit': 'createMilestone',
		'submit form': 'submit'
	},

	initialize: function(atts) {
		this.project = atts.project;
		this.milestones = atts.milestones;
	},

	render: function() {
		this.renderTemplate();
		this.form = new Backbone.Form({
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Milestone name (e.g. "v1.0" or "Sprint 1")' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
			}
		}).render();
		this.$el.find('.milestone-form').html(this.form.el);
		return this;
	},

	showForm: function(e) {
		e.preventDefault();
		this.$el.addClass('active');
	},

	hideForm: function() {
		this.$el.removeClass('active');
	},

	reset: function() {
		this.$el.find('form')[0].reset();
	},

	createMilestone: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			data.project_id = this.project.get('id');
			this.milestones.create(data, { wait: true });
			this.reset();
			this.hideForm();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.MilestoneView = Backbone.View.extend({

	id: 'milestone',
	className: 'milestone',
	template: App.Templates.Milestone,

	events: {
		'click .milestone-edit': 'editMilestone',
		'click .milestone-delete': 'deleteMilestone'
	},

	initialize: function() {
		var that = this;
		this.$el.appendTo('#content');

		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.removeMilestone);

		this.model.fetch({
			success: function(){
				App.Global.Events.trigger('update-project-select', { project_id: that.model.get('project_id') });
			},
			error: function(){
				var view = new App.Views.NotFoundView();
				$('#content').html(view.render().el);
			}
		});
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		var view = new App.Views.MilestoneTasksView({ model: this.model });
		this.$el.find('.milestone-body').append(view.render().el);
		return this;
	},

	editMilestone: function(e) {
		e.preventDefault();
		var modal = new App.Views.EditMilestoneModal({ model: this.model });
		$('body').append(modal.render().el);
	},

	deleteMilestone: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this milestone?')){
			this.projectId = this.model.get('project_id');
			this.model.destroy({ wait: true });
		}
	},

	removeMilestone: function() {
		this.remove();
		App.Router.navigate('#/project/'+ this.projectId, { trigger: true });
	}

});

App.Views.EditMilestoneModal = Backbone.Modal.extend({

	className: 'milestone-edit-modal',
	template: App.Templates.MilestoneEditModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'saveMilestone',
		'submit form': 'submit'
	},

	onRender: function() {
		this.form = new Backbone.Form({
			model: this.model,
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Milestone name' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
			}
		}).render();
		this.$el.find('.modal-content').html(this.form.el);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	saveMilestone: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			this.model.save(data, { wait: true });
			this.hideModal();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.MilestoneTasksView = Backbone.View.extend({

	tagName: 'ul',
	className: 'tasks',

	initialize: function(atts) {
		this.tasks = new App.Collections.Tasks({ model: App.Models.Task });

		this.listenTo(this.tasks, 'add', this.addOne);
		this.listenTo(this.tasks, 'reset', this.addAll);

		this.tasks.fetch({ data: { milestone_id: this.model.id } });

		var that = this;
		this.$el.sortable({
			items: 'li.task',
			revert: 100,
			axis: 'y',
			containment: 'parent',
			update: function(event, ui) {
				that.updateSort();
			}
		});
	},

	render: function() {
		var view = new App.Views.CreateTaskView({ milestone: this.model, tasks: this.tasks });
		this.$el.append(view.render().el);
		return this;
	},

	addOne: function(model) {
		var view = new App.Views.TaskListView({ model: model });
		if(model.get('completed') == false){
			view.render().$el.insertBefore(this.$el.find('.task-create'));
		} else {
			this.$el.append(view.render().el);
		}
	},

	addAll: function() {
		this.$el.html('');
		this.tasks.each(this.addOne, this);
	},

	updateSort: function() {
		var tasks = this.tasks;
			sorted = this.$el.sortable('toArray', { attribute: 'data-id' });

		_.each(sorted, function(id, index){
			var task = tasks.get(id);
			task.save({ 'sort': index });
		});
	}

});

App.Views.ProjectsView = Backbone.View.extend({

	id: 'projects',
	template: App.Templates.Projects,

	events: {
		'click .project-free-trial a': 'showFreeTrial'
	},

	initialize: function(atts) {
		this.projects = atts.collection;
		this.$el.appendTo('#content');

		this.listenTo(this.projects, 'add', this.addOne);
		this.listenTo(this.projects, 'reset', this.addAll);
		this.listenTo(this.projects, 'remove', this.removeProject);

		this.addAll(); // Already fetched

		var that = this;
		this.$el.find('.your-projects').sortable({
			items: 'li.project',
			revert: 100,
			containment: 'parent',
			update: function(event, ui) {
				that.updateSort();
			}
		});
	},

	render: function() {
		this.renderTemplate();
		if(App.Global.CurrentUser.get('role') != 'team'){
			var view = new App.Views.CreateProjectView({ projects: this.projects });
			this.$el.find('.your-projects').append(view.render().el);
			this.$el.find('.your-projects').append('<li class="project-import basecamp cf"><a href="/import/basecamp" class="open">Import from Basecamp</a></li>');
		} else {
			this.$el.find('.your-projects').append('<li class="project-free-trial cf"><a href="#" class="open">Start your Artisan Free Trial</a></li>');
		}
		this.toggleTitles();
		return this;
	},

	addOne: function(model) {
		this.$el.find('.project-import').hide();
		var parent = '.your-projects';
		if(model.get('owner') != App.Global.CurrentUser.get('id')) parent = '.shared-projects';

		var view = new App.Views.ProjectListView({ model: model });
		if(this.$el.find(parent +' .project').length){
			this.$el.find(parent +' .project').last().after(view.render().el);
		} else {
			this.$el.find(parent).prepend(view.render().el);
		}

		this.toggleTitles();
	},

	addAll: function() {
		this.render();
		this.projects.each(this.addOne, this);
	},

	removeProject: function() {
		if(!this.projects.length) this.$el.find('.project-import').show();
	},

	updateSort: function() {
		var projects = this.projects;
			sorted = this.$el.find('.your-projects').sortable('toArray', { attribute: 'data-id' });

		_.each(sorted, function(id, index){
			var project = projects.get(id);
			project.save({ 'sort': index });
		});

		projects.sort();
	},

	showFreeTrial: function(e) {
		e.preventDefault();
		var view = new App.Views.FreeTrialModal();
		$('body').append(view.render().el);
	},

	toggleTitles: function() {
		if(this.$el.find('.shared-projects li').length){
			this.$el.find('.title').show();
		} else {
			this.$el.find('.title').hide();
		}
	}

});

App.Views.ProjectListView = Backbone.View.extend({

	tagName: 'li',
	className: 'project',
	template: App.Templates.ProjectList,

	events: {
		'click .project-edit': 'editProject',
		'click .project-delete': 'deleteProject'
	},

	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.removeProject);
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		this.$el.attr('data-id', this.model.get('id')); // For sorting

		// Only owners can edit/delete projects
		if(this.model.get('owner') != App.Global.CurrentUser.get('id')){
			this.$el.find('.project-edit,.project-delete').remove();
		}

		return this;
	},

	editProject: function(e) {
		e.preventDefault();
		var modal = new App.Views.EditProjectModal({ model: this.model });
		$('body').append(modal.render().el);
	},

	deleteProject: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this project?')){
			this.model.destroy({ wait: true });
		}
	},

	removeProject: function() {
		this.remove();
	}

});

App.Views.CreateProjectView = Backbone.View.extend({

	tagName: 'li',
	className: 'project-create cf',

	events: {
		'click .open': 'showModal',
	},

	initialize: function(atts) {
		this.projects = atts.projects;
	},

	render: function() {
		this.$el.html('<a href="#" class="open">Create Project +</a>');
		return this;
	},

	showModal: function(e) {
		e.preventDefault();
		var modal = new App.Views.CreateProjectModal({ projects: this.projects });
		$('body').append(modal.render().el);
	}

});

App.Views.CreateProjectModal = Backbone.Modal.extend({

	className: 'project-create-modal',
	template: App.Templates.ProjectCreateModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'createProject',
		'submit form': 'submit'
	},

	initialize: function(atts) {
		this.projects = atts.projects;
	},

	onRender: function() {
		this.form = new Backbone.Form({
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Project name' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
			}
		}).render();
		this.$el.find('.modal-content').html(this.form.el);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	createProject: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			var that = this;
			this.projects.create(data, {
				wait: true,
				success: function() {
					that.hideModal();
				},
				error: function(collection, resp) {
					that.hideModal();
					if(resp.status == 403 && resp.responseJSON.message){ // 403 Forbidden = Need to upgrade
						var modal = new App.Views.UpgradeModal({ message: resp.responseJSON.message });
						$('body').append(modal.render().el);
					}
				}
			});
		}
	},

	submit: function(e) {
        e.preventDefault();
    }

});

App.Views.EditProjectModal = Backbone.Modal.extend({

	className: 'project-edit-modal',
	template: App.Templates.ProjectEditModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'saveProject',
		'submit form': 'submit'
	},

	onRender: function() {
		this.form = new Backbone.Form({
			model: this.model,
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Project name' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } },
				task_categories: {
					type: 'List',
					itemType: 'NestedModel',
					title: 'Categories',
					fieldClass: 'task-categories',
					editorClass: 'task-category',
					model: App.Models.TaskCategory,
					confirmDelete: 'Are you sure you want to delete this category?'
				},
				task_phases: {
					type: 'List',
					itemType: 'NestedModel',
					title: 'Phases',
					fieldClass: 'task-phases',
					editorClass: 'task-phase',
					model: App.Models.TaskPhase,
					confirmDelete: 'Are you sure you want to delete this phase?'
				},
			}
		}).render();
		this.$el.find('.modal-content').html(this.form.el);

		var categoriesEditor = this.form.fields.task_categories.editor;
		this.listenTo(categoriesEditor, 'add', this.addCategory);
		this.listenTo(categoriesEditor, 'item:change', this.saveCategory);
		this.listenTo(categoriesEditor, 'remove', this.removeCategory);

		var phasesEditor = this.form.fields.task_phases.editor;
		this.listenTo(phasesEditor, 'add', this.addPhase);
		this.listenTo(phasesEditor, 'item:change', this.savePhase);
		this.listenTo(phasesEditor, 'remove', this.removePhase);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	saveProject: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			this.model.save(data, { wait: true });
			this.form.commit();
			this.hideModal();
		}
	},

	addCategory: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		delete data.id;
		data.project_id = this.model.get('id');
		var category = new App.Models.TaskCategory();
		category.save(data, { wait: true });
		this.form.commit();
	},

	saveCategory: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		if(data.id){
			var category = new App.Models.TaskCategory({ id: data.id });
			category.save(data, { wait: true });
			this.form.commit();
		}
	},

	removeCategory: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		var category = new App.Models.TaskCategory({ id: data.id });
		category.destroy({ wait: true });
		this.form.commit();
	},

	addPhase: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		delete data.id;
		data.project_id = this.model.get('id');
		var phase = new App.Models.TaskPhase();
		phase.save(data, { wait: true });
		this.form.commit();
	},

	savePhase: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		if(data.id){
			var phase = new App.Models.TaskPhase({ id: data.id });
			phase.save(data, { wait: true });
			this.form.commit();
		}
	},

	removePhase: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		var phase = new App.Models.TaskPhase({ id: data.id });
		phase.destroy({ wait: true });
		this.form.commit();
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.ProjectView = Backbone.View.extend({

	tagName: 'ul',
	id: 'milestones',

	initialize: function() {
		var that = this;
		this.milestones = new App.Collections.Milestones({ model: App.Models.Milestone });
		this.$el.appendTo('#content');

		this.listenTo(this.milestones, 'add', this.addOne);
		this.listenTo(this.milestones, 'reset', this.addAll);

		this.model.fetch({
			success: function(){
				that.milestones.fetch({ data: { project_id: that.model.get('id') } });
				that.render();

				that.$el.sortable({
					items: 'li.milestone',
					revert: 100,
					axis: 'y',
					containment: 'parent',
					update: function(event, ui) {
						that.updateSort();
					}
				});

				App.Global.Events.trigger('update-project-select', { project_id: that.model.get('id') });
			},
			error: function(){
				var view = new App.Views.NotFoundView();
				$('#content').html(view.render().el);
			}
		});
	},

	render: function() {
		var view = new App.Views.CreateMilestoneView({ project: this.model, milestones: this.milestones });
		this.$el.append(view.render().el);
		return this;
	},

	addOne: function(model) {
		var view = new App.Views.MilestoneListView({ model: model });
		if(this.$el.find('.milestone-create').length){
			view.render().$el.insertBefore(this.$el.find('.milestone-create'));
		} else {
			this.$el.append(view.render().el);
		}
	},

	addAll: function() {
		this.$el.html('');
		this.milestones.each(this.addOne, this);
	},

	updateSort: function() {
		var milestones = this.milestones;
			sorted = this.$el.sortable('toArray', { attribute: 'data-id' });

		_.each(sorted, function(id, index){
			var milestone = milestones.get(id);
			milestone.save({ 'sort': index }, { silent: true });
		});
	}

});

App.Views.FreeTrialModal = Backbone.Modal.extend({

	className: 'free-trial-modal',
	template: App.Templates.FreeTrialModal,

	events: {
		'click .cancel': 'hideModal'
	},

	hideModal: function(e) {
		e.preventDefault();
		this.triggerCancel();
	}

});

App.Views.TaskListView = Backbone.View.extend({

	tagName: 'li',
	className: 'task cf',
	template: App.Templates.TaskList,

	events: {
		'click .completed': 'toggleComplete'
	},

	initialize: function() {
		this.listenTo(this.model, 'sync', this.render);
	},

	render: function() {
		this.renderTemplate(this.model.toJSON());
		this.$el.attr('data-id', this.model.get('id')); // For sorting

		if(this.model.get('completed') == true){
			this.$el.addClass('completed');
		} else {
			this.$el.removeClass('completed');
		}
		this.$el.find('.completed').attr('disabled', false);
		return this;
	},

	toggleComplete: function() {
		this.model.save({ completed: !this.model.get('completed') });
		this.$el.find('.completed').attr('disabled', true);
	}

});

App.Views.CreateTaskView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-create cf',

	events: {
		'focus input[name="name"]': 'showForm',
		'click .cancel': 'hideForm',
		'click .submit': 'createTask',
		'submit form': 'submit'
	},

	initialize: function(atts) {
		this.milestone = atts.milestone;
		this.tasks = atts.tasks;
	},

	render: function() {
		var project = App.Global.Projects.get(this.milestone.get('project_id')),
			categories = _.map(project.get('task_categories'), function(item){ return { val: item.id, label: item.name }; }),
			phases = _.map(project.get('task_phases'), function(item){ return { val: item.id, label: item.name }; }),
			users = _.map(project.get('users'), function(item){ return { val: item.id, label: item.name }; });

		categories.unshift({ val: '', label: 'No Category' });
		phases.unshift({ val: '', label: 'No Phase' });
		users.unshift({ val: '', label: 'Unassigned' });

		this.form = new Backbone.Form({
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Create task +' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } },
				category_id: { type: 'Select', options: categories, template: selectFieldTemplate },
				phase_id: { type: 'Select', options: phases, template: selectFieldTemplate },
				assigned_to: { type: 'Select', options: users, template: selectFieldTemplate },
				due_date: { type: 'Text', editorAttrs: { placeholder: 'Due date (optional)' } }
			}
		}).render();
		this.form.$el.find('input[name="due_date"]').datepicker({ dateFormat: 'yy-mm-dd' });
		this.$el.html(this.form.el);
		return this;
	},

	showForm: function() {
		this.$el.addClass('active');
	},

	hideForm: function() {
		this.$el.find('form')[0].reset();
		this.$el.removeClass('active');
	},

	createTask: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			data.project_id = this.milestone.get('project_id');
			data.milestone_id = this.milestone.get('id');
			data.completed = false;
			this.tasks.create(data, { wait: true });
			this.hideForm();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.TaskView = Backbone.View.extend({

	id: 'task',
	className: 'task',
	template: App.Templates.Task,

	events: {
		'click .task-complete': 'toggleComplete',
		'click .task-edit': 'editTask',
		'click .task-delete': 'deleteTask'
	},

	initialize: function() {
		var that = this;
		this.$el.appendTo('#content');

		this.listenTo(this.model, 'sync', this.render);
		this.listenTo(this.model, 'destroy', this.removeTask);

		this.model.fetch({
			success: function(){
				App.Global.Events.trigger('update-project-select', { project_id: that.model.get('project_id') });
			},
			error: function(){
				var view = new App.Views.NotFoundView();
				$('#content').html(view.render().el);
			}
		});
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		var $taskComplete = this.$el.find('.task-complete');
		if(this.model.get('completed') == true){
			this.$el.addClass('completed');
			$taskComplete.attr('title', 'Un-complete Task').addClass('icon-cross');
		} else {
			this.$el.removeClass('completed');
			$taskComplete.attr('title', 'Complete Task').addClass('icon-tick');
		}

		var commentsView = new App.Views.TaskCommentsView({ model: this.model });
		this.$el.find('.task-body').append(commentsView.render().el);
		return this;
	},

	toggleComplete: function(e) {
		e.preventDefault();
		this.model.save({ completed: !this.model.get('completed') });
	},

	editTask: function(e) {
		e.preventDefault();
		var modal = new App.Views.EditTaskModal({ model: this.model });
		$('body').append(modal.render().el);
	},

	deleteTask: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this task?')){
			this.milestoneId = this.model.get('milestone_id');
			this.model.destroy({ wait: true });
		}
	},

	removeTask: function() {
		this.remove();
		App.Router.navigate('#/milestone/'+ this.milestoneId, { trigger: true });
	}

});

App.Views.EditTaskModal = Backbone.Modal.extend({

	className: 'task-edit-modal',
	template: App.Templates.TaskEditModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'saveTask',
		'submit form': 'submit'
	},

	onRender: function() {
		var project = App.Global.Projects.get(this.model.get('project_id')),
			categories = _.map(project.get('task_categories'), function(item){ return { val: item.id, label: item.name }; }),
			phases = _.map(project.get('task_phases'), function(item){ return { val: item.id, label: item.name }; }),
			users = _.map(project.get('users'), function(item){ return { val: item.id, label: item.name }; });

		categories.unshift({ val: '', label: 'No Category' });
		phases.unshift({ val: '', label: 'No Phase' });
		users.unshift({ val: '', label: 'Unassigned' });

		this.form = new Backbone.Form({
			model: this.model,
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Create task +' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } },
				category_id: { type: 'Select', options: categories, template: selectFieldTemplate },
				phase_id: { type: 'Select', options: phases, template: selectFieldTemplate },
				assigned_to: { type: 'Select', options: users, template: selectFieldTemplate },
				due_date: { type: 'Text', editorAttrs: { placeholder: 'Due date (optional)' } }
			}
		}).render();
		this.form.$el.find('input[name="due_date"]').datepicker({ dateFormat: 'yy-mm-dd' });
		this.$el.find('.modal-content').html(this.form.el);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	saveTask: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			this.model.save(data, { wait: true });
			this.hideModal();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.TaskCommentsView = Backbone.View.extend({

	tagName: 'ul',
	className: 'task-comments',

	initialize: function() {
		this.comments = new App.Collections.TaskComments({ model: App.Models.TaskComment });

		this.listenTo(this.comments, 'add', this.addOne);
		this.listenTo(this.comments, 'reset', this.addAll);

		this.comments.fetch({ data: { task_id: this.model.id } });
	},

	render: function() {
		var view = new App.Views.CreateTaskCommentView({ task: this.model, comments: this.comments });
		this.$el.html(view.render().el);
		return this;
	},

	addOne: function(model) {
		var view = new App.Views.TaskCommentView({ model: model });
		view.render().$el.insertBefore(this.$el.find('.task-comment-create'));
	},

	addAll: function() {
		this.render();
		this.comments.each(this.addOne, this);
	}

});

App.Views.TaskCommentView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-comment',
	template: App.Templates.TaskComment,

	events: {
		'click .delete': 'deleteComment'
	},

	initialize: function() {
		this.listenTo(this.model, 'sync', this.render);
		this.listenTo(this.model, 'destroy', this.removeComment);
		this.listenTo(this.model, 'attachments-saved', this.renderAttachments);

		this.commentAttachmentsView = new App.Views.TaskCommentAttachmentsView({ model: this.model });
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		this.$el.find('.comment-content').html(this.model.get('content')); // Parse HTML
		this.$el.find('.comment-content a').attr('target', '_blank');
		this.$el.find('.comment-attachments').html(this.commentAttachmentsView.render().el);

		if(this.model.get('user_id') != App.Global.CurrentUser.get('id')){
			this.$el.find('.comment-delete').remove();
		}
		return this;
	},

	deleteComment: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this comment?')){
			this.$el.find('.delete').text('Deleting...');
			this.model.destroy({ wait: true });
		}
	},

	removeComment: function() {
		this.remove();
	},

	renderAttachments: function() {
		this.commentAttachmentsView.refresh();
	}

});

App.Views.CreateTaskCommentView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-comment-create cf',
	template: App.Templates.TaskCommentCreate,

	events: {
		'focus #task-comment-content': 'showForm',
		'click .cancel': 'cancelForm',
		'click .submit': 'createComment',
		'submit form': 'submit',
		'click .formatting': 'formattingHelp'
	},

	initialize: function(atts) {
		this.task = atts.task;
		this.comments = atts.comments;
		this.attachments = new App.Collections.TaskCommentAttachments();
	},

	render: function() {
		this.renderTemplate({ user: App.Global.CurrentUser.toJSON() });
		this.form = new Backbone.Form({
			schema: {
				content: { type: 'TextArea', validators: ['required'], editorAttrs: { id: 'task-comment-content', placeholder: 'Discuss this task...' } },
			}
		}).render();
		this.form.$el.append('<a href="#" class="formatting">Formatting Help</a>');
		this.form.$el.append('<a href="javascript:;" class="attach-files">Attach Files</a><ul class="attach-files-list"></ul>');
		this.$el.find('.comment-form').html(this.form.el);

		var uploader = new plupload.Uploader({
			browse_button: this.$el.find('.attach-files')[0],
			runtimes: 'html5,flash,silverlight',
			url: 'http://'+ awsData.bucket +'.s3.amazonaws.com',
			multipart: true,
			multipart_params: {
				'key': this.task.get('id') +'/${filename}', // use filename as a key
				'Filename': this.task.get('id') +'/${filename}', // adding this to keep consistency across the runtimes
				'acl': 'public-read',
				'Content-Type': '',
				'AWSAccessKeyId': awsData.accessKey,
				'policy': awsData.policy,
				'signature': awsData.signature
			},
			file_data_name: 'file', // optional, but better be specified directly
			unique_names: true,
			filters: {
				max_file_size : '200mb',
				mime_types: [
					{ title: 'Image files', extensions : 'jpg,jpeg,png,gif' },
					{ title: 'Document files', extensions : 'pdf,doc,docx,ppt,pptx,pps,ppsx,odt,xls,xlsx,zip,psd,ai,eps,svg' },
					{ title: 'Audio files', extensions : 'mp3,m4a,ogg,wav' },
					{ title: 'Video files', extensions : 'mp4,m4v,mov,wmv,avi,mpg,ogv' },
					{ title: 'Code files', extensions : 'actionscript,clojure,coffeescript,cpp,cs,css,diff,django,erlang,go,haml,handlebars,haskell,html,ini,java,js,javascript,json,lisp,lua,makefile,markdown,nginx,objectivec,perl,php,profile,python,ruby,scala,scss,sql,txt,xml' }
				]
			},
			flash_swf_url: '/bower_components/plupload/js/Moxie.swf',
			silverlight_xap_url: '/bower_components/plupload/js/Moxie.xap'
		});
		uploader.init();
		uploader.bind('FilesAdded', this.filesAdded, this);
		uploader.bind('Error', this.uploadError, this);
		uploader.bind('BeforeUpload', this.beforeUpload, this);
		uploader.bind('UploadProgress', this.uploadProgress, this);
		uploader.bind('FileUploaded', this.fileUploaded, this);
		uploader.bind('UploadComplete', this.uploadComplete, this);

		return this;
	},

	showForm: function() {
		this.$el.addClass('active');
	},

	cancelForm: function() {
		this.attachments.reset();
		this.hideForm();
	},

	hideForm: function() {
		this.$el.find('form')[0].reset();
		this.$el.removeClass('active');
		this.$el.find('.attach-files-list').empty();
	},

	createComment: function() {
		if(!this.form.validate()){
			var that = this,
				data = this.form.getValue();
			data.task_id = this.task.get('id');
			this.comments.create(data, {
				wait: true,
				success: function(model, resp) {
					that.saveAttachments(model);
				}
			});
			this.hideForm();
		}
	},

	filesAdded: function(up, files) {
		var filesList = this.$el.find('.attach-files-list');
		_.each(files, function(file) {
			var extension = file.name.split('.').pop(),
				filename = this.task.get('id') +'/'+ file.id +'.'+ extension,
				data = {
					name: file.name,
					filename: filename,
					size: file.size,
					type: file.type
				};
			var attachment = this.attachments.add(data);
			var view = new App.Views.CreateTaskCommentAttachmentView({ model: attachment, collection: this.attachments, file_id: file.id });
			filesList.append(view.render().el);
		}, this);
		up.start();
		this.$el.find('.btn').attr('disabled', true);
		this.$el.find('.attach-files').text('Uploading...');
		up.disableBrowse(true);
	},

	uploadError: function(up, err) {
		this.$el.find('.attach-files').text('Attach Files');
		alert(err.message);
	},

	beforeUpload: function(up, file) {
		// Hack to make unique_names work
		var file_name = file.name;
		var extension = file.name.split('.').pop();
		up.settings.multipart_params.key = this.task.get('id') +'/'+ file.id +'.'+ extension;
		up.settings.multipart_params.Filename = this.task.get('id') +'/'+ file.id +'.'+ extension;
	},

	uploadProgress: function(up, file) {
		this.$el.find('#'+ file.id +' .progress').text(file.percent +'%');
	},

	fileUploaded: function(up, file) {
		this.$el.find('#'+ file.id).addClass('uploaded');
	},

	uploadComplete: function(up, file) {
		this.$el.find('.btn').attr('disabled', false);
		this.$el.find('.attach-files').text('Attach Files');
		up.disableBrowse(false);
	},

	saveAttachments: function(comment) {
		var that = this;
		if(this.attachments.length){
			this.attachments.each(function(attachment){
				attachment.set('comment_id', comment.get('id'));
				attachment.save({ wait: true }).done(function(){
					comment.trigger('attachments-saved');
					that.attachments.remove(attachment);
				});
			});

		}
	},

	submit: function(e) {
		e.preventDefault();
	},

	formattingHelp: function(e) {
		e.preventDefault();
		var view = new App.Views.FormattingHelpModal();
		$('body').append(view.render().el);
	}

});

App.Views.TaskCommentAttachmentsView = Backbone.View.extend({

	tagName: 'ul',
	className: 'task-comment-attachments',

	initialize: function() {
		this.attachments = new App.Collections.TaskCommentAttachments();

		this.listenTo(this.attachments, 'add', this.addOne);
		this.listenTo(this.attachments, 'reset', this.addAll);

		this.attachments.fetch({ data: { comment_id: this.model.get('id') } });
	},

	addOne: function(model) {
		var view = new App.Views.TaskCommentAttachmentView({ model: model });
		this.$el.append(view.render().el);
	},

	addAll: function() {
		this.$el.html('');
		this.attachments.each(this.addOne, this);
	},

	refresh: function() {
		this.attachments.fetch({
			data: { comment_id: this.model.get('id') },
			remove: false
		});
	}

});

App.Views.TaskCommentAttachmentView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-comment-attachment',
	template: App.Templates.TaskCommentAttachment,

	initialize: function() {
		this.listenTo(this.model, 'sync', this.render);
	},

	render: function() {
		var file_type = 'normal';
		if(_.contains(['image/gif','image/jpeg','image/png'], this.model.get('type'))){
			file_type = 'image';
		}

		var data = this.model.toJSON();
		data.file_type = file_type;
		data.extension = this.model.get('name').split('.').pop();
		this.$el.html(this.template(data));
		return this;
	},

});

App.Views.CreateTaskCommentAttachmentView = Backbone.View.extend({

	tagName: 'li',
	className: 'create-task-comment-attachment',
	template: App.Templates.TaskCommentAttachmentCreate,

	events: {
		'click .delete-attachment': 'deleteAttachment'
	},

	initialize: function(atts) {
		this.file_id = atts.file_id;

		this.listenTo(this.model, 'sync', this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.attr('id', this.file_id);
		this.$el.addClass(this.model.get('name').split('.').pop());
		return this;
	},

	deleteAttachment: function(e) {
		e.preventDefault();
		this.collection.remove(this.model);
		this.remove();
	}

});

App.Views.FormattingHelpModal = Backbone.Modal.extend({

	className: 'formatting-help-modal',
	template: App.Templates.FormattingHelp,

	events: {
		'click .cancel': 'hideModal'
	},

	hideModal: function(e) {
		e.preventDefault();
		this.triggerCancel();
	}

});

(function(App, $, Backbone){

    $(function(){

        /**
         * Routes
         */
        App.Router.on('route:index', function() {
            $('#content').html('Loading...');
        });

        App.Router.on('route:projects', function(id) {
            $('#content').html('');
            new App.Views.ProjectsView({ collection: App.Global.Projects });
        });

        App.Router.on('route:project', function(id) {
            $('#content').html('');
            var model = new App.Models.Project({ id: id });
            new App.Views.ProjectView({ model: model });
            $('.header .project-select option[value="'+ id +'"]').attr('selected', true);
        });

        App.Router.on('route:milestone', function(id) {
            $('#content').html('');
            var model = new App.Models.Milestone({ id: id });
            new App.Views.MilestoneView({ model: model });
        });

        App.Router.on('route:task', function(id) {
            $('#content').html('');
            var model = new App.Models.Task({ id: id });
            new App.Views.TaskView({ model: model });
        });

        App.Router.on('route:activity', function(id) {
            $('#content').html('');
            new App.Views.HistoryView();
        });

        /**
         * Misc Views
         */
        App.Views.ProjectSelectView = Backbone.View.extend({

            className: 'project-select',
            template: App.Templates.ProjectSelect,

            events: {
                'change select': 'changeProject'
            },

            initialize: function() {
                this.listenTo(App.Global.Projects, 'add', this.render);
                this.listenTo(App.Global.Projects, 'change', this.render);
                this.listenTo(App.Global.Projects, 'remove', this.render);

                App.Global.Events.on('update-project-select', this.updateSelect, this);
            },

            render: function(){
                this.renderTemplate({ projects: App.Global.Projects.toJSON() });
                if(this.currentProject){
                    this.$el.find('option[value="'+ this.currentProject.id +'"]').attr('selected', true);
                }
                return this;
            },

            changeProject: function(){
                var selectedProject = this.$el.find('option:selected').val();
                if(selectedProject == 'all'){
                    this.currentProject = null;
                    App.Router.navigate('#/projects', { trigger: true });
                } else {
                    this.currentProject = App.Global.Projects.get(selectedProject);
                    App.Router.navigate('#/project/'+ this.currentProject.id, { trigger: true });
                }
            },

            updateSelect: function(data){
                if(typeof data.project_id !== 'undefined'){
                    this.currentProject = App.Global.Projects.get(data.project_id);
                    this.$el.find('option[value="'+ this.currentProject.id +'"]').attr('selected', true);
                }
            }

        });

        App.Views.UpgradeModal = Backbone.Modal.extend({

            className: 'upgrade-modal',
            template: App.Templates.UpgradeModal,

            events: {
                'click .cancel': 'hideModal'
            },

            initialize: function(atts) {
                this.message = atts.message;
            },

            onRender: function() {
                this.$el.find('.modal-content').html('<p class="message">'+ this.message +'</p>');
            },

            hideModal: function(e) {
                e.preventDefault();
                this.triggerCancel();
            }

        });

        App.Views.NotFoundView = Backbone.View.extend({

            className: 'not-found',
            template: App.Templates.NotFound

        });

        /**
         * Main View
         */
        App.Views.MainView = Backbone.View.extend({

            initialize: function() {
                _.bindAll(this, 'projectsLoaded');

                App.Global.Projects = new App.Collections.Projects({ model: App.Models.Project });
                App.Global.Projects.fetch({
                    success: this.projectsLoaded,
                });

                App.Global.CurrentUser = new App.Models.User(currentUser);

                new App.Views.ActivityMenuView();
            },

            projectsLoaded: function(){
                Backbone.history.start();

                var route = Backbone.history.fragment;
                if(!route) route = '#/projects';

                var projectSelect = new App.Views.ProjectSelectView();
                $('.header').append(projectSelect.render().el);
                App.Router.navigate(route, { trigger: true });
            }

        });
        new App.Views.MainView();

    });

}(App, jQuery, Backbone));
