(function($){

	$(function(){

		if($('#upgrade-form').length){

			var btnText = $('#upgrade-form .btn').val();

			var stripeResponseHandler = function(status, response) {
				var $form = $('#upgrade-form');

				if(response.error){
					$form.before('<p class="error">'+ response.error.message +'</p>');
					$form.find('.btn').prop('disabled', false).val(btnText);
				} else {
					var token = response.id;
					$form.append($('<input type="hidden" name="token" />').val(token));
					$form.get(0).submit();
				}
			};

			$('#upgrade-form').submit(function(event) {
				var $form = $(this);
				$('.error').remove();
				$form.find('.btn').prop('disabled', true).val('Processing...');
				Stripe.card.createToken($form, stripeResponseHandler);
				return false;
			});

		}

		if($('#billing-edit-form').length){



		}

		if($('#billing-cancel-form').length){

			$('#billing-cancel-form').submit(function(event) {
				var $form = $(this);
				if(confirm('Are you sure you want to cancel your subscription?')){
					$form.find('.btn').prop('disabled', true).val('Processing...');
					return true;
				}
				return false;
			});

		}

	});

}(jQuery));
