<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskCategory extends Model
{
    protected $fillable = array(
		'project_id',
		'name',
		'description'
	);

	protected static $rules = array(
		'project_id' => 'required',
		'name' => 'required'
	);

	public function project()
	{
		return $this->belongsTo('App\Project');
	}
}
