<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';

	protected $fillable = array(
		'project_id',
		'user_id',
		'type',
		'type_id',
		'action',
		'content'
	);

	protected static $rules = array(
		'project_id' => 'required',
		'user_id' => 'required',
		'action' => 'required'
	);

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function project()
	{
		return $this->belongsTo('App\Project');
	}
}
