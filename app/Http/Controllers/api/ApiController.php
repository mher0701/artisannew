<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class ApiController extends Controller
{
    protected $user;

	public function __construct(Request $request)
	{
		$this->middleware('auth');

		$user = null;
		$api_key = $request->header('X-API-KEY');
		if($api_key) $user = User::where('api_key', $api_key)->first();

		if(!$user){
			header('Content-Type: application/json');
			header('HTTP/1.1 401 Unauthorized');
			echo json_encode(array(
				'error' => array(
					'code' => 401,
					'message' => 'Unauthorized'
				)
			), 401);
			exit;
		} else {
			$this->user = $user;
		}
	}
}
