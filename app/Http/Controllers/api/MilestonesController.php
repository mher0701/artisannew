<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Milestone;
use App\History;

use Response;
use Input;

class MilestonesController extends ApiController
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$milestones = Milestone::orderBy('sort')->get();

		$milestones = $milestones->filter(function($milestone) use ($projects){
			return $projects->contains($milestone->project_id);
		})->values();

		if(Input::get('project_id')){
			$milestones = $milestones->filter(function($milestone){
			    return $milestone->project_id == Input::get('project_id');
			})->values();
		}

		return Response::json($milestones);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$projects = User::find($this->user->id)->projects;
		if(!$projects->contains(Input::get('project_id'))){
			return Response::json(array('code' => '412', 'message' => 'Invalid project_id'), 412);
		}

		$milestones = Milestone::where('project_id', Input::get('project_id'))->orderBy('sort')->get();
		$milestones = $milestones->filter(function($milestone) use ($projects){
			return $projects->contains($milestone->project_id);
		})->values();

		$milestone = new Milestone(Input::all());
		if($milestones->count()) $milestone->sort = ($milestones->last()->sort + 1);

		if($milestone->save()){
			$milestone->load(array('project'));

			History::create(array(
				'project_id' => $milestone->project_id,
				'user_id' => $this->user->id,
				'type' => 'milestone',
				'type_id' => $milestone->id,
				'action' => 'created',
				'content' => $milestone->toJSON()
			));
			return Response::json($milestone);
		} else {
			return Response::json(array('code' => '412', 'message' => $milestone->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$milestone = Milestone::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($milestone->project_id)) return App::abort(403, 'Unauthorized.');
		return Response::json($milestone);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projects = User::find($this->user->id)->projects;
		$milestone = Milestone::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($milestone->project_id)) return App::abort(403, 'Unauthorized.');

		if($milestone->update(Input::all())){
			History::create(array(
				'project_id' => $milestone->project_id,
				'user_id' => $this->user->id,
				'type' => 'milestone',
				'type_id' => $milestone->id,
				'action' => 'updated',
				'content' => $milestone->toJSON()
			));
			return Response::json($milestone);
		} else {
			return Response::json(array('code' => '412', 'message' => $milestone->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$projects = User::find($this->user->id)->projects;
		$milestone = Milestone::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($milestone->project_id)) return App::abort(403, 'Unauthorized.');

		History::create(array(
			'project_id' => $milestone->project_id,
			'user_id' => $this->user->id,
			'type' => 'milestone',
			'type_id' => $milestone->id,
			'action' => 'deleted',
			'content' => $milestone->toJSON()
		));
		return Response::json($milestone->delete());
	}

}
