<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\TaskCategory;
use App\Task;
use App\TaskComment;
use App\History;
use App\TaskPhase;

use Response;
use Input;
use App;

class TaskPhasesController extends ApiController
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$phases = TaskPhase::orderBy('project_id')->get();

		$phases = $phases->filter(function($phase) use ($projects){
			return $projects->contains($phase->project_id);
		})->values();

		if(Input::get('project_id')){
			$phases = $phases->filter(function($phase){
				return $phase->project_id == Input::get('project_id');
			})->values();
		}

		return Response::json($phases);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$projects = User::find($this->user->id)->projects;
		if(!$projects->contains(Input::get('project_id'))){
			return Response::json(array('code' => '412', 'message' => 'Invalid project_id'), 412);
		}

		$phase = new TaskPhase(Input::all());
		if($phase->save()){
			return Response::json($phase);
		} else {
			return Response::json(array('code' => '412', 'message' => $phase->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$phase = TaskPhase::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($phase->project_id)) return App::abort(403, 'Unauthorized.');
		return Response::json($phase);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projects = User::find($this->user->id)->projects;
		$phase = TaskPhase::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($phase->project_id)) return App::abort(403, 'Unauthorized.');

		if($phase->update(Input::all())){
			return Response::json($phase);
		} else {
			return Response::json(array('code' => '412', 'message' => $phase->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$projects = User::find($this->user->id)->projects;
		$phase = TaskPhase::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($phase->project_id)) return App::abort(403, 'Unauthorized.');

		return Response::json($phase->delete());
	}
}
