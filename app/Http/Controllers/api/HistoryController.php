<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\History;

use Response;
use Input;

class HistoryController extends ApiController
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$history = History::with(array('user','project'))->orderBy('created_at', 'desc')->take(50)->get();

		$history = $history->filter(function($item) use ($projects){
			return ($projects->contains($item->project_id));
		})->values();

		if(Input::get('limit')){
			$history = $history->slice(0, filter_var(Input::get('limit'), FILTER_SANITIZE_NUMBER_INT))->values();
		}

		if(Input::get('project_id')){
			$history = $history->filter(function($item){
				return $item->project_id == Input::get('project_id');
			})->values();
		}

		return Response::json($history);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$history = History::where('id', $id)->with(array('user','project'))->firstOrFail();
		if(!$projects->contains($history->project_id)) return App::abort(403, 'Unauthorized.');
		return Response::json($history);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
