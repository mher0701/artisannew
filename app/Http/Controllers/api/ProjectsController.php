<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Project;
use App\TaskCategory;
use App\TaskPhase;

use App;
use Input;

class ProjectsController extends ApiController
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$projects->load('taskCategories','taskPhases','users');
		return response()->json($projects);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if($this->user->role == 'team') return App::abort(403, 'Unauthorized.');

		$projects = User::find($this->user->id)->projects()->where('owner', $this->user->id)->orderBy('sort')->get();
		if((($this->user->stripe_plan == '' || $this->user->stripe_plan == 'artisan_startup') && $projects->count() >= 5) ||
		   ($this->user->stripe_plan == 'artisan_agency' && $projects->count() >= 15) ||
		   ($this->user->stripe_plan == 'artisan_business' && $projects->count() >= 50)){
			return response()->json(array('code' => '403', 'message' => 'You have used up your project quota. Upgrade to create more projects.'), 403);
		}

		$project = new Project(Input::all());
		$project->owner = $this->user->id;
		if($projects->count()) $project->sort = ($projects->last()->sort + 1);

		if($project->save()){
			$project->users()->attach($this->user->id);

			// Default categories
			TaskCategory::create(array('project_id' => $project->id, 'name' => 'Bug', 'description' => 'Bugs are known problems with your code that need fixed.'));
			TaskCategory::create(array('project_id' => $project->id, 'name' => 'Issue', 'description' => 'Issues are potential problems that need investigated or researched.'));
			TaskCategory::create(array('project_id' => $project->id, 'name' => 'Feature', 'description' => 'Features are enhancements you want to add to your codebase.'));
			// Default phases
			TaskPhase::create(array('project_id' => $project->id, 'name' => 'Design'));
			TaskPhase::create(array('project_id' => $project->id, 'name' => 'Development'));
			TaskPhase::create(array('project_id' => $project->id, 'name' => 'Testing'));

			$project->load('taskCategories','taskPhases','users');
			return response()->json($project);
		} else {
			return response()->json(array('code' => '412', 'message' => $project->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$project = User::find($this->user->id)->project($id)->firstOrFail();
		return response()->json($project);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if($this->user->role == 'team') return App::abort(403, 'Unauthorized.');

		$project = User::find($this->user->id)->project($id)->firstOrFail();
		if($project->owner != $this->user->id) return App::abort(403, 'Unauthorized.');

		if($project->update(Input::all())){
			$project->load('taskCategories','taskPhases');
			return response()->json($project);
		} else {
			return response()->json(array('code' => '412', 'message' => $project->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if($this->user->role == 'team') return App::abort(403, 'Unauthorized.');

		$project = User::find($this->user->id)->project($id)->firstOrFail();
		if($project->owner != $this->user->id) return App::abort(403, 'Unauthorized.');

		$project->users()->detach($this->user->id);
		return response()->json($project->delete());
	}
}
