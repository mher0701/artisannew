<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Task;
use App\History;

use Response;
use Input;

class UsersController extends ApiController
{
    public function __construct()
    {
		parent::__construct();
        // $this->beforeFilter('auth.admin');
        $this->middleware('admin');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
		return Response::json($users);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = new User(Input::all());
		$user->api_key = sha1($user->id . $user->email . time());

		if($user->save()){
			return Response::json($user);
		} else {
			return Response::json(array('code' => '412', 'message' => $user->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);
		return Response::json($user);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::findOrFail($id);

		$input = Input::only('email','name','role');

		if($user->update($input)){
			return Response::json($user);
		} else {
			return Response::json(array('code' => '412', 'message' => $user->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->projects()->detach();
		return Response::json($user->delete());
	}
}
