<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Task;
use App\History;

use Response;
use Input;

class TasksController extends ApiController
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::with(array('category','phase','assignedUser','createdBy'))->orderBy('sort')->get();

		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		if(Input::get('milestone_id')){
			$tasks = $tasks->filter(function($task){
				return $task->milestone_id == Input::get('milestone_id');
			})->values();
		}

		return Response::json($tasks);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$projects = User::find($this->user->id)->projects;
		if(!$projects->contains(Input::get('project_id'))){
			return Response::json(array('code' => '412', 'message' => 'Invalid project_id'), 412);
		}

		$tasks = Task::where('milestone_id', Input::get('milestone_id'))->orderBy('sort')->get();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		// dd(Input::all());
		$task = new Task(Input::all());
		$task->created_by = $this->user->id;
		if($tasks->count()) $task->sort = ($tasks->last()->sort + 1);
		if($task->due_date){
			if($timestamp = strtotime($task->due_date)){
				$task->due_date = date('Y-m-d H:i:s', $timestamp);
			} else {
				$task->due_date = '';
			}
		}

		if($task->save()){
			$task->load(array('category','phase','assignedUser','createdBy'));

			History::create(array(
				'project_id' => $task->project_id,
				'user_id' => $this->user->id,
				'type' => 'task',
				'type_id' => $task->id,
				'action' => 'created',
				'content' => $task->toJSON()
			));
			return Response::json($task);
		} else {
			return Response::json(array('code' => '412', 'message' => $task->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$task = Task::where('id', $id)->with(array('category','phase','assignedUser','createdBy','comments' => function($query){
			$query->with('user')->orderBy('created_at', 'asc');
		},'milestone'=> function($query){
			$query->with('project');
		}))->firstOrFail();
		if(!$projects->contains($task->project_id)) return App::abort(403, 'Unauthorized.');
		return Response::json($task);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projects = User::find($this->user->id)->projects;
		$task = Task::where('id', $id)->firstOrFail();
		if(!$projects->contains($task->project_id)) return App::abort(403, 'Unauthorized.');

		$input = Input::all();
		if(isset($input['assigned_user'])) unset($input['assigned_user']);
		if(isset($input['category'])) unset($input['category']);
		if(isset($input['comments'])) unset($input['comments']);
		if(isset($input['created_by'])) unset($input['created_by']);
		if(isset($input['milestone'])) unset($input['milestone']);
		if(isset($input['phase'])) unset($input['phase']);

		if($task->update($input)){
			$task->load(array('category','phase','assignedUser','createdBy'));

			History::create(array(
				'project_id' => $task->project_id,
				'user_id' => $this->user->id,
				'type' => 'task',
				'type_id' => $task->id,
				'action' => 'updated',
				'content' => $task->toJSON()
			));
			return Response::json($task);
		} else {
			return Response::json(array('code' => '412', 'message' => $task->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$projects = User::find($this->user->id)->projects;
		$task = Task::where('id', $id)->firstOrFail();
		if(!$projects->contains($task->project_id)) return App::abort(403, 'Unauthorized.');

		History::create(array(
			'project_id' => $task->project_id,
			'user_id' => $this->user->id,
			'type' => 'task',
			'type_id' => $task->id,
			'action' => 'deleted',
			'content' => $task->toJSON()
		));
		return Response::json($task->delete());
	}
}
