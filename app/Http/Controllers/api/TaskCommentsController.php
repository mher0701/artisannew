<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\TaskCategory;
use App\Task;
use App\TaskComment;
use App\History;

use Response;
use Input;
use App;

class TaskCommentsController extends ApiController
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comments = TaskComment::with(array('user','attachments'))->orderBy('created_at', 'asc')->get();
		$comments = $comments->filter(function($comment) use ($tasks){
			return ($tasks->contains($comment->task_id));
		})->values();

		if(Input::get('task_id')){
			$comments = $comments->filter(function($comment){
				return $comment->task_id == Input::get('task_id');
			})->values();
		}

		return Response::json($comments);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();

		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		if(!$tasks->contains(Input::get('task_id'))){
			return Response::json(array('code' => '412', 'message' => 'Invalid task_id'), 412);
		}

		$data = Input::all();
		$data['content'] = strip_tags($data['content']);
		$comment = new TaskComment($data);
		$comment->user_id = $this->user->id;

		if($comment->save()){
			$comment->load(array('user','attachments'));

			History::create(array(
				'project_id' => $comment->task->project->id,
				'user_id' => $this->user->id,
				'type' => 'comment',
				'type_id' => $comment->id,
				'action' => 'created',
				'content' => $comment->toJSON()
			));
			return Response::json($comment);
		} else {
			return Response::json(array('code' => '412', 'message' => $comment->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comment = TaskComment::where('id', $id)->with(array('user','attachments'))->firstOrFail();
		if(!$tasks->contains($comment->task_id)) return App::abort(403, 'Unauthorized.');

		return Response::json($comment);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comment = TaskComment::where('id', $id)->firstOrFail();
		if(!$tasks->contains($comment->task_id)) return App::abort(403, 'Unauthorized.');

		if($comment->update(Input::all())){
			$comment->load(array('user','attachments'));

			History::create(array(
				'project_id' => $comment->task->project->id,
				'user_id' => $this->user->id,
				'type' => 'comment',
				'type_id' => $comment->id,
				'action' => 'updated',
				'content' => $comment->toJSON()
			));
			return Response::json($comment);
		} else {
			return Response::json(array('code' => '412', 'message' => $comment->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comment = TaskComment::where('id', $id)->firstOrFail();
		if(!$tasks->contains($comment->task_id)) return App::abort(403, 'Unauthorized.');
		if($comment->user_id != $this->user->id) return App::abort(403, 'Unauthorized.');

		History::create(array(
			'project_id' => $comment->task->project->id,
			'user_id' => $this->user->id,
			'type' => 'comment',
			'type_id' => $comment->id,
			'action' => 'deleted',
			'content' => $comment->toJSON()
		));
		return Response::json($comment->delete());
	}
}
