<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;

class AdminController extends BaseController
{
	public function __construct()
	{
		$this->middleware('admin');
	}

    public function getIndex()
	{
		return View::make('admin.index');
	}
}
