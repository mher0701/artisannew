<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use Input;
use Validator;
use Auth;
use Redirect;

class AccountController extends BaseController
{
	public function getAppPage(){
		return view('app.main');
	}

    public function getIndex()
	{
		return View::make('app.account')->with(array('user' => Auth::user(), 'page_title' => 'Manage Account'));
	}

	public function postInfo()
	{
		$user = Auth::user();
		$data = array(
			'name' => Input::get('name'),
			'email' => Input::get('email')
		);

		$validator = Validator::make($data, array(
			'name' => 'required',
			'email' => 'required|email|unique:users,email,'. $user->id
		));
		if($validator->fails()){
			return redirect()->back()->withErrors($validator);
		}

		if(!$user->update($data)){
			return redirect()->back()->withErrors($user->getErrors());
		}

		return redirect()->back()->withStatus('Account info saved');
	}

	public function postPassword()
	{
		$user = Auth::user();
		$data = array(
			'password' => Input::get('password'),
			'password_confirmation' => Input::get('password_confirmation')
		);

		$validator = Validator::make($data, array(
			'password' => 'required|confirmed'
		));
		if($validator->fails()){
			return redirect()->back()->withErrors($validator);
		}

		if(!$user->update(array('password' => $data['password']))){
			return redirect()->back()->withErrors($user->getErrors());
		}

		return redirect()->back()->withStatus('Account password changed');
	}
}
