<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebhookController extends Controller
{
    public function handleWebhook()
    {
        // Handle other events...

        // Fallback to failed payment check...
        $payload = $this->getJsonPayload();
		if(isset($payload['type'])){
			switch($payload['type'])
			{
				case 'invoice.payment_failed':
					return $this->handleFailedPayment($payload);
			}
		}
    }
}
