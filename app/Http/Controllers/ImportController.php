<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App;
use View;
use OAuth;
use Session;
use Input;
use Redirect;
use URL;

use App\Project;
use App\TaskCategory;
use App\TaskPhase;
use App\Milestone;
use App\Task;

class ImportController extends BaseController
{
    public function basecamp()
	{
		if(Auth::user()->role == 'team') return App::abort(404);
		
		return View::make('app.import.basecamp')->with(array('page_title' => 'Import from Basecamp'));
	}

	public function basecampLogin()
	{
		$basecamp = OAuth::consumer('Basecamp', URL::to('import/basecamp/login'));
		if(!Session::has('basecamp_token')){
			$code = Input::get('code');
		    if(!empty($code)){
			    $token = $basecamp->requestAccessToken($code);
				Session::put('basecamp_token', $token);
			} else {
			    $url = $basecamp->getAuthorizationUri();
			    return Redirect::to((string)$url);
			}
		}

		$result = json_decode($basecamp->request('https://launchpad.37signals.com/authorization.json'), true);
		if(isset($result['accounts']) && !empty($result['accounts'])){
			$formattedAccounts = array();
			foreach($result['accounts'] as $account){
				if($account['product'] == 'bcx') $formattedAccounts[$account['id']] = $account['name'];
			}
			return View::make('app.import.basecamp')->with(array(
				'accounts' => $formattedAccounts,
				'page_title' => 'Import from Basecamp'
			));
		} else {
			return Redirect::to('import/basecamp')->withError('Unable to retrieve any active basecamp accounts.');
		}
	}

	public function doImport()
	{
		$basecamp = OAuth::consumer('Basecamp', URL::to('import/basecamp/login'));
		if(!Session::has('basecamp_token')){
			$code = Input::get('code');
			if(!empty($code)){
				$token = $basecamp->requestAccessToken($code);
				Session::put('basecamp_token', $token);
			} else {
				$url = $basecamp->getAuthorizationUri();
				return Redirect::to((string)$url);
			}
		}

		$account_id = filter_var(Input::get('account'), FILTER_SANITIZE_NUMBER_INT);
		$projects = json_decode($basecamp->request('https://basecamp.com/'. $account_id .'/api/v1/projects.json'), true);
		if(!empty($projects)){
			foreach($projects as $bc_project){
				$project = Project::firstOrNew(array(
					'owner' => Auth::user()->id,
					'name' => $bc_project['name'],
					'description' => $bc_project['description'] ? $bc_project['description'] : '',
					'sort' => 0
				));
				if(!$project->exists && $project->save()){
					$project->users()->attach(Auth::user()->id);

					// Default categories
					TaskCategory::create(array('project_id' => $project->id, 'name' => 'Bug', 'description' => 'Bugs are known problems with your code that need fixed.'));
					TaskCategory::create(array('project_id' => $project->id, 'name' => 'Issue', 'description' => 'Issues are potential problems that need investigated or researched.'));
					TaskCategory::create(array('project_id' => $project->id, 'name' => 'Feature', 'description' => 'Features are enhancements you want to add to your codebase.'));
					// Default phases
					TaskPhase::create(array('project_id' => $project->id, 'name' => 'Design'));
					TaskPhase::create(array('project_id' => $project->id, 'name' => 'Development'));
					TaskPhase::create(array('project_id' => $project->id, 'name' => 'Testing'));
				}

				$milestones = json_decode($basecamp->request('https://basecamp.com/'. $account_id .'/api/v1/projects/'. $bc_project['id'] .'/todolists.json'), true);
				if(!empty($milestones)){
					foreach($milestones as $bc_milestone){
						$milestone = Milestone::firstOrNew(array(
							'project_id' => $project->id,
							'name' => $bc_milestone['name'],
							'description' => $bc_milestone['description'] ? $bc_milestone['description'] : '',
							'sort' => $bc_milestone['position']
						));
						if(!$milestone->exists){
							$milestone->save();
						}

						$tasks = json_decode($basecamp->request('https://basecamp.com/'. $account_id .'/api/v1/projects/'. $bc_project['id'] .'/todolists/'. $bc_milestone['id'] .'.json'), true);
						if(isset($tasks['todos']['remaining']) && !empty($tasks['todos']['remaining'])){
							foreach($tasks['todos']['remaining'] as $bc_task){
								$task = Task::firstOrNew(array(
									'project_id' => $project->id,
									'milestone_id' => $milestone->id,
									'name' => $bc_task['content'],
									'created_by' => Auth::user()->id,
									'due_date' => $bc_task['due_at'] ? date('Y-m-d H:i:s', strtotime($bc_task['due_at'])) : '',
									'sort' => $bc_task['position']
								));
								if(!$task->exists){
									$task->save();
								}
							}
						}
						if(isset($tasks['todos']['completed']) && !empty($tasks['todos']['completed'])){
							foreach($tasks['todos']['completed'] as $bc_task){
								$task = Task::firstOrNew(array(
									'project_id' => $project->id,
									'milestone_id' => $milestone->id,
									'name' => $bc_task['content'],
									'created_by' => Auth::user()->id,
									'due_date' => $bc_task['due_at'] ? date('Y-m-d H:i:s', strtotime($bc_task['due_at'])) : '',
									'sort' => $bc_task['position'],
									'completed' => true
								));
								if(!$task->exists){
									$task->save();
								}
							}
						}
					}
				}
			}
		}

		Session::forget('basecamp_token');
		return Redirect::to('/');
	}
}
