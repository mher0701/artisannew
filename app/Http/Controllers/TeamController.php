<?php

namespace App\Http\Controllers;

use Auth;
use Route;
use View;
use Input;
use Redirect;
use Validator;
use Mail;
use Request;
use Response;
use App;

use App\Project;
use App\User;
use App\Hashids;

class TeamController extends BaseController
{
    public function index($project_id=null)
	{
		if(Auth::user()->role == 'team') return App::abort(404);

		// $project_id = Route::input('project_id');
		$projects = Project::where('owner', Auth::user()->id)->get();

		$current_project = null;
		if($project_id && $projects->count()){
			$current_project = $projects->filter(function($project) use ($project_id){
				return $project->id == $project_id;
			})->first();
		}
		if(!$current_project) $current_project = $projects->first();

		$can_add_users = true;
		if($current_project){
			$current_project->load('users');

			$user = Auth::user();
			if((($user->stripe_plan == '' || $user->stripe_plan == 'artisan_startup') && $current_project->users->count() >= 3) ||
			   ($user->stripe_plan == 'artisan_agency' && $current_project->users->count() >= 9) ||
			   ($user->stripe_plan == 'artisan_business' && $current_project->users->count() >= 18)){
				$can_add_users = false;
			}
		}

		return View::make('app.team')->with(array(
			'projects' => $projects,
			'current_project' => $current_project,
			'can_add_users' => $can_add_users,
			'page_title' => 'Manage Team'
		));
	}

	public function addUser()
	{
		$project_id = Input::get('project_id');
		$project = Project::where('id', $project_id)->where('owner', Auth::user()->id)->firstOrFail();

		$user = Auth::user();
		if((($user->stripe_plan == '' || $user->stripe_plan == 'artisan_startup') && $project->users->count() >= 3) ||
		   ($user->stripe_plan == 'artisan_agency' && $project->users->count() >= 9) ||
		   ($user->stripe_plan == 'artisan_business' && $current_project->users->count() >= 18)){
			return Redirect::to('team/'. $project_id)->withError('You have used up your quota of users for this team. Please upgrade.');
		}

		$validator = Validator::make(Input::only('email'), array(
			'email' => 'required|email'
		));
		if($validator->fails()){
			return Redirect::to('team/'. $project_id)->withErrors($validator);
		}

		$user = User::firstOrNew(array('email' => Input::get('email')));
		if(!$user->exists){
			$user->name = Input::get('name') ? Input::get('name') : 'Unknown';
			$user->api_key = sha1($user->id . $user->email . time());
			$user->role = 'team';
			$user->status = 'invited';
			$user->password = str_random(10) . time();
			$user->used_free_trial = false;

			if($user->save()){
				$hashids = new Hashids();
				$token = $hashids->encrypt($user->id);
				Mail::send('emails.invite_new', array('user' => $user, 'token' => $token, 'invited_by' => Auth::user()), function($message) use ($user){
					$message->to($user->email, $user->name)
					->from('hi@artisan.so', 'Artisan')
					->subject('You\'ve been invited to join Artisan');
				});
			} else {
				return Redirect::to('team/'. $project_id)->withError('Invalid name or email address.');
			}
		} else {
			Mail::send('emails.invite', array('user' => $user, 'invited_by' => Auth::user(), 'project' => $project), function($message) use ($user, $project){
				$message->to($user->email, $user->name)
				->from('hi@artisan.so', 'Artisan')
				->subject('You\'ve been invited to '. $project->name .' on Artisan');
			});
		}

		$user->projects()->attach($project_id);

		return Redirect::to('team/'. $project_id);
	}

	public function removeUser()
	{
		if(!Request::ajax()) return Redirect::to('team');

		$project_id = Input::get('project_id');
		$user_id = Input::get('user_id');
		$project = Project::where('id', $project_id)->where('owner', Auth::user()->id)->firstOrFail();
		$project->load('users');
		$project->users()->detach($user_id);

		return Response::json(array('success' => true));
	}
}
