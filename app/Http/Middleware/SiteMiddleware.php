<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class SiteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            return Redirect::to('app');
        }
        return $next($request);
    }
}
