<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskCommentAttachment extends Model
{
    protected $fillable = array(
		'comment_id',
		'name',
		'filename',
		'size',
		'type',
		'uploaded_by'
	);

	protected static $rules = array(
		'comment_id' => 'required',
		'name' => 'required',
		'filename' => 'required',
		'type' => 'required'
	);

	public function comment()
	{
		return $this->belongsTo('App\TaskComment', 'comment_id');
	}

	public function uploadedByUser()
	{
		return $this->belongsTo('App\User', 'uploaded_by');
	}

	/*public static function boot()
    {
        parent::boot();

        static::deleted(function($attachment){
			$s3 = AWS::get('s3');
			$s3->deleteObject(array(
				'Bucket' => 'artisanapp',
				'Key' => $attachment->filename
			));
			return true;
		});
	}*/
}
