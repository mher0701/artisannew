<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = array(
		'owner',
		'name',
		'description',
		'sort'
	);

	protected static $rules = array(
        'owner' => 'required',
		'name' => 'required'
    );

	public function milestones()
	{
		return $this->hasMany('App\Milestone');
	}

	public function tasks()
    {
        return $this->hasMany('App\Task');
    }

	public function taskCategories()
	{
		return $this->hasMany('App\TaskCategory');
	}

	public function taskPhases()
	{
		return $this->hasMany('App\TaskPhase');
	}

	public function users()
	{
		return $this->belongsToMany('App\User')->withTimestamps();
	}

	public function delete()
	{
		$this->milestones()->delete();
		$this->taskCategories()->delete();
		$this->taskPhases()->delete();
		return parent::delete();
	}
}
