<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Hash;
use Laravel\Cashier\Billable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
    protected $cardUpFront = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'name',
        'api_key',
        'remember_token',
        'role',
        'status',
        'used_free_trial',
        'card_brand'
    ];

    protected static $rules = array(
        'email' => 'required|email',
        'name' => 'required',
        'password' => 'required'
    );

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = array('password', 'api_key', 'remember_token');

    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array_merge($this->attributes, array(
            'trial_ends_at' => \Carbon\Carbon::now()->addDays(14)
        )), true);
        parent::__construct($attributes);
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
    * Get the API key for the user.
    *
    * @return string
    */
    public function getAuthApiKey()
    {
        return $this->api_key;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }

    public function project($project_id)
    {
        return $this->belongsToMany('App\Project')->where('project_user.project_id', $project_id);
    }

    public function onGracePeriod()
    {
        if ( ! is_null($endsAt = $this->subscription_ends_at))
        {
            return Carbon::today()->lt(Carbon::instance($endsAt));
        }
        else
        {
            return false;
        }
    }
}
