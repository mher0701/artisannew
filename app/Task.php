<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = array(
		'project_id',
		'milestone_id',
		'name',
		'description',
		'category_id',
		'phase_id',
		'assigned_to',
		'created_by',
		'due_date',
		'sort',
		'completed'
	);

	protected $touches = array('milestone');

	protected static $rules = array(
        'project_id' => 'required',
		'name' => 'required'
    );

	public function project()
    {
        return $this->belongsTo('App\Project');
    }

	public function milestone()
	{
		return $this->belongsTo('App\Milestone');
	}

	public function category()
	{
		return $this->belongsTo('App\TaskCategory', 'category_id');
	}

	public function phase()
	{
		return $this->belongsTo('App\TaskPhase', 'phase_id');
	}

	public function assignedUser()
	{
		return $this->belongsTo('App\User', 'assigned_to');
	}

	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}

	public function comments()
	{
		return $this->hasMany('App\TaskComment');
	}

	public function delete()
    {
        $this->comments()->delete();
        return parent::delete();
    }

	public function getCompletedAttribute($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
}
