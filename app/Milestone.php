<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    protected $fillable = array(
		'project_id',
		'name',
		'description',
		'sort'
	);

	protected $touches = array('project');

	protected static $rules = array(
        'project_id' => 'required',
		'name' => 'required'
    );

	public function project()
    {
        return $this->belongsTo('App\Project');
    }

	public function tasks()
	{
		return $this->hasMany('App\Task');
	}

	public function delete()
	{
		// TODO Delete task comments
		$this->tasks()->delete();
		return parent::delete();
	}
}
