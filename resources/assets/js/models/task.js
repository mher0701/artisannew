App.Models.Task = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/tasks'

});

App.Models.TaskComment = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-comments'

});

App.Models.TaskCommentAttachment = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-comment-attachments'

});

App.Models.TaskCategory = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-categories',

	schema: {
		id: { type: 'Hidden' },
		name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Category name' } },
		description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
	},

	toString: function() {
        return this.get('name');
    }

});

App.Models.TaskPhase = Backbone.Model.extend({

	urlRoot: App.Config.apiRoot +'/task-phases',

	schema: {
		id: { type: 'Hidden' },
		name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Phase name' } },
		description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
	},

	toString: function() {
		return this.get('name');
	}

});
