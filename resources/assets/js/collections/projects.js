App.Collections.Projects = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/projects',
	model: App.Models.Project,
	comparator: function(model) {
		return model.get('sort');
	}

});
