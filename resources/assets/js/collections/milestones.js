App.Collections.Milestones = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/milestones',
	model: App.Models.Milestone,
	comparator: function(model) {
		return model.get('sort');
	}

});
