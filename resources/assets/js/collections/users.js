App.Collections.Users = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/users',
	model: App.Models.User

});
