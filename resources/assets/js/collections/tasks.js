App.Collections.Tasks = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/tasks',
	model: App.Models.Task,
	comparator: function(model) {
        return model.get('sort');
    }

});

App.Collections.TaskComments = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/task-comments',
	model: App.Models.TaskComment

});

App.Collections.TaskCommentAttachments = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/task-comment-attachments',
	model: App.Models.TaskCommentAttachment

});

App.Collections.TaskCategories = Backbone.Model.extend({

	url: App.Config.apiRoot +'/task-categories',
	model: App.Models.TaskComment

});

App.Collections.TaskPhases = Backbone.Model.extend({

	url: App.Config.apiRoot +'/task-phases',
	model: App.Models.TaskComment

});
