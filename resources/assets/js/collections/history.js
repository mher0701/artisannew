App.Collections.History = Backbone.Collection.extend({

	url: App.Config.apiRoot +'/history',
	model: App.Models.History

});
