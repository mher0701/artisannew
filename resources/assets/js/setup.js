// Global App
window.App = {
	Config: {
		apiRoot: '/api/1'
	},
	Models: {},
	Collections: {},
	Views: {},
	Router: {},
	Sync: {},
	Templates: {},
	Global: {}
};

/*
* Store a version of Backbone.sync to call from the
* modified version we create
*/
App.Sync.backboneSync = Backbone.sync;
Backbone.sync = function(method, model, options) {

	options.headers = {
		'X-API-KEY': currentUser.apiKey
	};

	/*
	* Call the stored original Backbone.sync method with
	* extra headers argument added
	*/
	return App.Sync.backboneSync(method, model, options);

};

/*
 * Router
 */
var AppRouter = Backbone.Router.extend({

	routes: {
		'': 'index',
		'projects': 'projects',
		'project/:id': 'project',
		'milestone/:id': 'milestone',
		'task/:id': 'task',
		'activity': 'activity'
	}

});
App.Router = new AppRouter();

/*
 * Global events pub/sub
 */
App.Global.Events = _.extend({}, Backbone.Events);

/*
 * Backbone Form templates
 */
if(typeof Backbone.Form !== 'undefined'){
	Backbone.Form.template = _.template('<form class="backbone-form"><div data-fieldsets></div><input type="button" value="Cancel" class="btn alt cancel"> <input type="button" value="Save" class="btn submit"></form>');
	var defaultFormTemplate = _.template('<form data-fieldsets></form>');
	var selectFieldTemplate = _.template('<div class="styled-select" data-editor></div>');
}

/*
 * Underscore template helpers
 */
function gravatar(email, size){
	var s = (typeof size !== "undefined") ? size : 40;
	return "https://www.gravatar.com/avatar/" + md5( email ) + "?s="+ s +"&d=mm";
}

function formatSize(size) {
	if (typeof size === 'undefined' || /\D/.test(size)) {
		return 'N/A';
	}
	function round(num, precision) {
		return Math.round(num * Math.pow(10, precision)) / Math.pow(10, precision);
	}
	var boundary = Math.pow(1024, 4);
	// TB
	if (size > boundary) {
		return round(size / boundary, 1) + ' tb';
	}
	// GB
	if (size > (boundary/=1024)) {
		return round(size / boundary, 1) + ' gb';
	}
	// MB
	if (size > (boundary/=1024)) {
		return round(size / boundary, 1) + ' mb';
	}
	// KB
	if (size > 1024) {
		return Math.round(size / 1024) + ' kb';
	}
	return size + ' b';
}

function stripHTML(html)
{
	var tmp = document.createElement("DIV");
	tmp.innerHTML = html;
	return tmp.textContent || tmp.innerText || "";
}

/*
 * Handlebars template helpers
 */
if(typeof Handlebars !== 'undefined'){
	// Handlebars.js - Gravatar thumbnail
	// Usage: {{#gravatar email size="64"}}{{/gravatar}} [depends on md5.js]
	// Author: Makis Tracend (@tracend)
	Handlebars.registerHelper('gravatar', function(context, options) {
		var email = context;
		var size=( typeof(options.hash.size) === "undefined") ? 32 : options.hash.size;
		return "https://www.gravatar.com/avatar/" + md5( email ) + "?s="+ size +"&d=mm";
	});

	Handlebars.registerHelper('formatDate', function(datetime, options) {
		if(datetime == '' || datetime == '0000-00-00 00:00:00') return '';
		var dateFormats = {
			'default': 'D MMM YYYY',
			'relative': ''
		};
		if(moment){
			if(!options.hash.format) options.hash.format = 'default';
			if(options.hash.format == 'relative'){
				return moment(datetime).fromNow();
			} else {
				f = dateFormats[options.hash.format];
				return moment(datetime).format(f);
			}
		}
		return datetime;
	});
}
