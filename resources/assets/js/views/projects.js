App.Views.ProjectsView = Backbone.View.extend({

	id: 'projects',
	template: App.Templates.Projects,

	events: {
		'click .project-free-trial a': 'showFreeTrial'
	},

	initialize: function(atts) {
		this.projects = atts.collection;
		this.$el.appendTo('#content');

		this.listenTo(this.projects, 'add', this.addOne);
		this.listenTo(this.projects, 'reset', this.addAll);
		this.listenTo(this.projects, 'remove', this.removeProject);

		this.addAll(); // Already fetched

		var that = this;
		this.$el.find('.your-projects').sortable({
			items: 'li.project',
			revert: 100,
			containment: 'parent',
			update: function(event, ui) {
				that.updateSort();
			}
		});
	},

	render: function() {
		this.renderTemplate();
		if(App.Global.CurrentUser.get('role') != 'team'){
			var view = new App.Views.CreateProjectView({ projects: this.projects });
			this.$el.find('.your-projects').append(view.render().el);
			this.$el.find('.your-projects').append('<li class="project-import basecamp cf"><a href="/import/basecamp" class="open">Import from Basecamp</a></li>');
		} else {
			this.$el.find('.your-projects').append('<li class="project-free-trial cf"><a href="#" class="open">Start your Artisan Free Trial</a></li>');
		}
		this.toggleTitles();
		return this;
	},

	addOne: function(model) {
		this.$el.find('.project-import').hide();
		var parent = '.your-projects';
		if(model.get('owner') != App.Global.CurrentUser.get('id')) parent = '.shared-projects';

		var view = new App.Views.ProjectListView({ model: model });
		if(this.$el.find(parent +' .project').length){
			this.$el.find(parent +' .project').last().after(view.render().el);
		} else {
			this.$el.find(parent).prepend(view.render().el);
		}

		this.toggleTitles();
	},

	addAll: function() {
		this.render();
		this.projects.each(this.addOne, this);
	},

	removeProject: function() {
		if(!this.projects.length) this.$el.find('.project-import').show();
	},

	updateSort: function() {
		var projects = this.projects;
			sorted = this.$el.find('.your-projects').sortable('toArray', { attribute: 'data-id' });

		_.each(sorted, function(id, index){
			var project = projects.get(id);
			project.save({ 'sort': index });
		});

		projects.sort();
	},

	showFreeTrial: function(e) {
		e.preventDefault();
		var view = new App.Views.FreeTrialModal();
		$('body').append(view.render().el);
	},

	toggleTitles: function() {
		if(this.$el.find('.shared-projects li').length){
			this.$el.find('.title').show();
		} else {
			this.$el.find('.title').hide();
		}
	}

});

App.Views.ProjectListView = Backbone.View.extend({

	tagName: 'li',
	className: 'project',
	template: App.Templates.ProjectList,

	events: {
		'click .project-edit': 'editProject',
		'click .project-delete': 'deleteProject'
	},

	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.removeProject);
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		this.$el.attr('data-id', this.model.get('id')); // For sorting

		// Only owners can edit/delete projects
		if(this.model.get('owner') != App.Global.CurrentUser.get('id')){
			this.$el.find('.project-edit,.project-delete').remove();
		}

		return this;
	},

	editProject: function(e) {
		e.preventDefault();
		var modal = new App.Views.EditProjectModal({ model: this.model });
		$('body').append(modal.render().el);
	},

	deleteProject: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this project?')){
			this.model.destroy({ wait: true });
		}
	},

	removeProject: function() {
		this.remove();
	}

});

App.Views.CreateProjectView = Backbone.View.extend({

	tagName: 'li',
	className: 'project-create cf',

	events: {
		'click .open': 'showModal',
	},

	initialize: function(atts) {
		this.projects = atts.projects;
	},

	render: function() {
		this.$el.html('<a href="#" class="open">Create Project +</a>');
		return this;
	},

	showModal: function(e) {
		e.preventDefault();
		var modal = new App.Views.CreateProjectModal({ projects: this.projects });
		$('body').append(modal.render().el);
	}

});

App.Views.CreateProjectModal = Backbone.Modal.extend({

	className: 'project-create-modal',
	template: App.Templates.ProjectCreateModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'createProject',
		'submit form': 'submit'
	},

	initialize: function(atts) {
		this.projects = atts.projects;
	},

	onRender: function() {
		this.form = new Backbone.Form({
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Project name' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
			}
		}).render();
		this.$el.find('.modal-content').html(this.form.el);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	createProject: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			var that = this;
			this.projects.create(data, {
				wait: true,
				success: function() {
					that.hideModal();
				},
				error: function(collection, resp) {
					that.hideModal();
					if(resp.status == 403 && resp.responseJSON.message){ // 403 Forbidden = Need to upgrade
						var modal = new App.Views.UpgradeModal({ message: resp.responseJSON.message });
						$('body').append(modal.render().el);
					}
				}
			});
		}
	},

	submit: function(e) {
        e.preventDefault();
    }

});

App.Views.EditProjectModal = Backbone.Modal.extend({

	className: 'project-edit-modal',
	template: App.Templates.ProjectEditModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'saveProject',
		'submit form': 'submit'
	},

	onRender: function() {
		this.form = new Backbone.Form({
			model: this.model,
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Project name' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } },
				task_categories: {
					type: 'List',
					itemType: 'NestedModel',
					title: 'Categories',
					fieldClass: 'task-categories',
					editorClass: 'task-category',
					model: App.Models.TaskCategory,
					confirmDelete: 'Are you sure you want to delete this category?'
				},
				task_phases: {
					type: 'List',
					itemType: 'NestedModel',
					title: 'Phases',
					fieldClass: 'task-phases',
					editorClass: 'task-phase',
					model: App.Models.TaskPhase,
					confirmDelete: 'Are you sure you want to delete this phase?'
				},
			}
		}).render();
		this.$el.find('.modal-content').html(this.form.el);

		var categoriesEditor = this.form.fields.task_categories.editor;
		this.listenTo(categoriesEditor, 'add', this.addCategory);
		this.listenTo(categoriesEditor, 'item:change', this.saveCategory);
		this.listenTo(categoriesEditor, 'remove', this.removeCategory);

		var phasesEditor = this.form.fields.task_phases.editor;
		this.listenTo(phasesEditor, 'add', this.addPhase);
		this.listenTo(phasesEditor, 'item:change', this.savePhase);
		this.listenTo(phasesEditor, 'remove', this.removePhase);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	saveProject: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			this.model.save(data, { wait: true });
			this.form.commit();
			this.hideModal();
		}
	},

	addCategory: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		delete data.id;
		data.project_id = this.model.get('id');
		var category = new App.Models.TaskCategory();
		category.save(data, { wait: true });
		this.form.commit();
	},

	saveCategory: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		if(data.id){
			var category = new App.Models.TaskCategory({ id: data.id });
			category.save(data, { wait: true });
			this.form.commit();
		}
	},

	removeCategory: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		var category = new App.Models.TaskCategory({ id: data.id });
		category.destroy({ wait: true });
		this.form.commit();
	},

	addPhase: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		delete data.id;
		data.project_id = this.model.get('id');
		var phase = new App.Models.TaskPhase();
		phase.save(data, { wait: true });
		this.form.commit();
	},

	savePhase: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		if(data.id){
			var phase = new App.Models.TaskPhase({ id: data.id });
			phase.save(data, { wait: true });
			this.form.commit();
		}
	},

	removePhase: function(listEditor, itemEditor) {
		var data = itemEditor.getValue();
		var phase = new App.Models.TaskPhase({ id: data.id });
		phase.destroy({ wait: true });
		this.form.commit();
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.ProjectView = Backbone.View.extend({

	tagName: 'ul',
	id: 'milestones',

	initialize: function() {
		var that = this;
		this.milestones = new App.Collections.Milestones({ model: App.Models.Milestone });
		this.$el.appendTo('#content');

		this.listenTo(this.milestones, 'add', this.addOne);
		this.listenTo(this.milestones, 'reset', this.addAll);

		this.model.fetch({
			success: function(){
				that.milestones.fetch({ data: { project_id: that.model.get('id') } });
				that.render();

				that.$el.sortable({
					items: 'li.milestone',
					revert: 100,
					axis: 'y',
					containment: 'parent',
					update: function(event, ui) {
						that.updateSort();
					}
				});

				App.Global.Events.trigger('update-project-select', { project_id: that.model.get('id') });
			},
			error: function(){
				var view = new App.Views.NotFoundView();
				$('#content').html(view.render().el);
			}
		});
	},

	render: function() {
		var view = new App.Views.CreateMilestoneView({ project: this.model, milestones: this.milestones });
		this.$el.append(view.render().el);
		return this;
	},

	addOne: function(model) {
		var view = new App.Views.MilestoneListView({ model: model });
		if(this.$el.find('.milestone-create').length){
			view.render().$el.insertBefore(this.$el.find('.milestone-create'));
		} else {
			this.$el.append(view.render().el);
		}
	},

	addAll: function() {
		this.$el.html('');
		this.milestones.each(this.addOne, this);
	},

	updateSort: function() {
		var milestones = this.milestones;
			sorted = this.$el.sortable('toArray', { attribute: 'data-id' });

		_.each(sorted, function(id, index){
			var milestone = milestones.get(id);
			milestone.save({ 'sort': index }, { silent: true });
		});
	}

});

App.Views.FreeTrialModal = Backbone.Modal.extend({

	className: 'free-trial-modal',
	template: App.Templates.FreeTrialModal,

	events: {
		'click .cancel': 'hideModal'
	},

	hideModal: function(e) {
		e.preventDefault();
		this.triggerCancel();
	}

});
