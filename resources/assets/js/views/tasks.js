App.Views.TaskListView = Backbone.View.extend({

	tagName: 'li',
	className: 'task cf',
	template: App.Templates.TaskList,

	events: {
		'click .completed': 'toggleComplete'
	},

	initialize: function() {
		this.listenTo(this.model, 'sync', this.render);
	},

	render: function() {
		this.renderTemplate(this.model.toJSON());
		this.$el.attr('data-id', this.model.get('id')); // For sorting

		if(this.model.get('completed') == true){
			this.$el.addClass('completed');
		} else {
			this.$el.removeClass('completed');
		}
		this.$el.find('.completed').attr('disabled', false);
		return this;
	},

	toggleComplete: function() {
		this.model.save({ completed: !this.model.get('completed') });
		this.$el.find('.completed').attr('disabled', true);
	}

});

App.Views.CreateTaskView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-create cf',

	events: {
		'focus input[name="name"]': 'showForm',
		'click .cancel': 'hideForm',
		'click .submit': 'createTask',
		'submit form': 'submit'
	},

	initialize: function(atts) {
		this.milestone = atts.milestone;
		this.tasks = atts.tasks;
	},

	render: function() {
		var project = App.Global.Projects.get(this.milestone.get('project_id')),
			categories = _.map(project.get('task_categories'), function(item){ return { val: item.id, label: item.name }; }),
			phases = _.map(project.get('task_phases'), function(item){ return { val: item.id, label: item.name }; }),
			users = _.map(project.get('users'), function(item){ return { val: item.id, label: item.name }; });

		categories.unshift({ val: '', label: 'No Category' });
		phases.unshift({ val: '', label: 'No Phase' });
		users.unshift({ val: '', label: 'Unassigned' });

		this.form = new Backbone.Form({
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Create task +' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } },
				category_id: { type: 'Select', options: categories, template: selectFieldTemplate },
				phase_id: { type: 'Select', options: phases, template: selectFieldTemplate },
				assigned_to: { type: 'Select', options: users, template: selectFieldTemplate },
				due_date: { type: 'Text', editorAttrs: { placeholder: 'Due date (optional)' } }
			}
		}).render();
		this.form.$el.find('input[name="due_date"]').datepicker({ dateFormat: 'yy-mm-dd' });
		this.$el.html(this.form.el);
		return this;
	},

	showForm: function() {
		this.$el.addClass('active');
	},

	hideForm: function() {
		this.$el.find('form')[0].reset();
		this.$el.removeClass('active');
	},

	createTask: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			data.project_id = this.milestone.get('project_id');
			data.milestone_id = this.milestone.get('id');
			data.completed = false;
			this.tasks.create(data, { wait: true });
			this.hideForm();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.TaskView = Backbone.View.extend({

	id: 'task',
	className: 'task',
	template: App.Templates.Task,

	events: {
		'click .task-complete': 'toggleComplete',
		'click .task-edit': 'editTask',
		'click .task-delete': 'deleteTask'
	},

	initialize: function() {
		var that = this;
		this.$el.appendTo('#content');

		this.listenTo(this.model, 'sync', this.render);
		this.listenTo(this.model, 'destroy', this.removeTask);

		this.model.fetch({
			success: function(){
				App.Global.Events.trigger('update-project-select', { project_id: that.model.get('project_id') });
			},
			error: function(){
				var view = new App.Views.NotFoundView();
				$('#content').html(view.render().el);
			}
		});
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		var $taskComplete = this.$el.find('.task-complete');
		if(this.model.get('completed') == true){
			this.$el.addClass('completed');
			$taskComplete.attr('title', 'Un-complete Task').addClass('icon-cross');
		} else {
			this.$el.removeClass('completed');
			$taskComplete.attr('title', 'Complete Task').addClass('icon-tick');
		}

		var commentsView = new App.Views.TaskCommentsView({ model: this.model });
		this.$el.find('.task-body').append(commentsView.render().el);
		return this;
	},

	toggleComplete: function(e) {
		e.preventDefault();
		this.model.save({ completed: !this.model.get('completed') });
	},

	editTask: function(e) {
		e.preventDefault();
		var modal = new App.Views.EditTaskModal({ model: this.model });
		$('body').append(modal.render().el);
	},

	deleteTask: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this task?')){
			this.milestoneId = this.model.get('milestone_id');
			this.model.destroy({ wait: true });
		}
	},

	removeTask: function() {
		this.remove();
		App.Router.navigate('#/milestone/'+ this.milestoneId, { trigger: true });
	}

});

App.Views.EditTaskModal = Backbone.Modal.extend({

	className: 'task-edit-modal',
	template: App.Templates.TaskEditModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'saveTask',
		'submit form': 'submit'
	},

	onRender: function() {
		var project = App.Global.Projects.get(this.model.get('project_id')),
			categories = _.map(project.get('task_categories'), function(item){ return { val: item.id, label: item.name }; }),
			phases = _.map(project.get('task_phases'), function(item){ return { val: item.id, label: item.name }; }),
			users = _.map(project.get('users'), function(item){ return { val: item.id, label: item.name }; });

		categories.unshift({ val: '', label: 'No Category' });
		phases.unshift({ val: '', label: 'No Phase' });
		users.unshift({ val: '', label: 'Unassigned' });

		this.form = new Backbone.Form({
			model: this.model,
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Create task +' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } },
				category_id: { type: 'Select', options: categories, template: selectFieldTemplate },
				phase_id: { type: 'Select', options: phases, template: selectFieldTemplate },
				assigned_to: { type: 'Select', options: users, template: selectFieldTemplate },
				due_date: { type: 'Text', editorAttrs: { placeholder: 'Due date (optional)' } }
			}
		}).render();
		this.form.$el.find('input[name="due_date"]').datepicker({ dateFormat: 'yy-mm-dd' });
		this.$el.find('.modal-content').html(this.form.el);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	saveTask: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			this.model.save(data, { wait: true });
			this.hideModal();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.TaskCommentsView = Backbone.View.extend({

	tagName: 'ul',
	className: 'task-comments',

	initialize: function() {
		this.comments = new App.Collections.TaskComments({ model: App.Models.TaskComment });

		this.listenTo(this.comments, 'add', this.addOne);
		this.listenTo(this.comments, 'reset', this.addAll);

		this.comments.fetch({ data: { task_id: this.model.id } });
	},

	render: function() {
		var view = new App.Views.CreateTaskCommentView({ task: this.model, comments: this.comments });
		this.$el.html(view.render().el);
		return this;
	},

	addOne: function(model) {
		var view = new App.Views.TaskCommentView({ model: model });
		view.render().$el.insertBefore(this.$el.find('.task-comment-create'));
	},

	addAll: function() {
		this.render();
		this.comments.each(this.addOne, this);
	}

});

App.Views.TaskCommentView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-comment',
	template: App.Templates.TaskComment,

	events: {
		'click .delete': 'deleteComment'
	},

	initialize: function() {
		this.listenTo(this.model, 'sync', this.render);
		this.listenTo(this.model, 'destroy', this.removeComment);
		this.listenTo(this.model, 'attachments-saved', this.renderAttachments);

		this.commentAttachmentsView = new App.Views.TaskCommentAttachmentsView({ model: this.model });
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		this.$el.find('.comment-content').html(this.model.get('content')); // Parse HTML
		this.$el.find('.comment-content a').attr('target', '_blank');
		this.$el.find('.comment-attachments').html(this.commentAttachmentsView.render().el);

		if(this.model.get('user_id') != App.Global.CurrentUser.get('id')){
			this.$el.find('.comment-delete').remove();
		}
		return this;
	},

	deleteComment: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this comment?')){
			this.$el.find('.delete').text('Deleting...');
			this.model.destroy({ wait: true });
		}
	},

	removeComment: function() {
		this.remove();
	},

	renderAttachments: function() {
		this.commentAttachmentsView.refresh();
	}

});

App.Views.CreateTaskCommentView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-comment-create cf',
	template: App.Templates.TaskCommentCreate,

	events: {
		'focus #task-comment-content': 'showForm',
		'click .cancel': 'cancelForm',
		'click .submit': 'createComment',
		'submit form': 'submit',
		'click .formatting': 'formattingHelp'
	},

	initialize: function(atts) {
		this.task = atts.task;
		this.comments = atts.comments;
		this.attachments = new App.Collections.TaskCommentAttachments();
	},

	render: function() {
		this.renderTemplate({ user: App.Global.CurrentUser.toJSON() });
		this.form = new Backbone.Form({
			schema: {
				content: { type: 'TextArea', validators: ['required'], editorAttrs: { id: 'task-comment-content', placeholder: 'Discuss this task...' } },
			}
		}).render();
		this.form.$el.append('<a href="#" class="formatting">Formatting Help</a>');
		this.form.$el.append('<a href="javascript:;" class="attach-files">Attach Files</a><ul class="attach-files-list"></ul>');
		this.$el.find('.comment-form').html(this.form.el);

		var uploader = new plupload.Uploader({
			browse_button: this.$el.find('.attach-files')[0],
			runtimes: 'html5,flash,silverlight',
			url: 'http://'+ awsData.bucket +'.s3.amazonaws.com',
			multipart: true,
			multipart_params: {
				'key': this.task.get('id') +'/${filename}', // use filename as a key
				'Filename': this.task.get('id') +'/${filename}', // adding this to keep consistency across the runtimes
				'acl': 'public-read',
				'Content-Type': '',
				'AWSAccessKeyId': awsData.accessKey,
				'policy': awsData.policy,
				'signature': awsData.signature
			},
			file_data_name: 'file', // optional, but better be specified directly
			unique_names: true,
			filters: {
				max_file_size : '200mb',
				mime_types: [
					{ title: 'Image files', extensions : 'jpg,jpeg,png,gif' },
					{ title: 'Document files', extensions : 'pdf,doc,docx,ppt,pptx,pps,ppsx,odt,xls,xlsx,zip,psd,ai,eps,svg' },
					{ title: 'Audio files', extensions : 'mp3,m4a,ogg,wav' },
					{ title: 'Video files', extensions : 'mp4,m4v,mov,wmv,avi,mpg,ogv' },
					{ title: 'Code files', extensions : 'actionscript,clojure,coffeescript,cpp,cs,css,diff,django,erlang,go,haml,handlebars,haskell,html,ini,java,js,javascript,json,lisp,lua,makefile,markdown,nginx,objectivec,perl,php,profile,python,ruby,scala,scss,sql,txt,xml' }
				]
			},
			flash_swf_url: '/bower_components/plupload/js/Moxie.swf',
			silverlight_xap_url: '/bower_components/plupload/js/Moxie.xap'
		});
		uploader.init();
		uploader.bind('FilesAdded', this.filesAdded, this);
		uploader.bind('Error', this.uploadError, this);
		uploader.bind('BeforeUpload', this.beforeUpload, this);
		uploader.bind('UploadProgress', this.uploadProgress, this);
		uploader.bind('FileUploaded', this.fileUploaded, this);
		uploader.bind('UploadComplete', this.uploadComplete, this);

		return this;
	},

	showForm: function() {
		this.$el.addClass('active');
	},

	cancelForm: function() {
		this.attachments.reset();
		this.hideForm();
	},

	hideForm: function() {
		this.$el.find('form')[0].reset();
		this.$el.removeClass('active');
		this.$el.find('.attach-files-list').empty();
	},

	createComment: function() {
		if(!this.form.validate()){
			var that = this,
				data = this.form.getValue();
			data.task_id = this.task.get('id');
			this.comments.create(data, {
				wait: true,
				success: function(model, resp) {
					that.saveAttachments(model);
				}
			});
			this.hideForm();
		}
	},

	filesAdded: function(up, files) {
		var filesList = this.$el.find('.attach-files-list');
		_.each(files, function(file) {
			var extension = file.name.split('.').pop(),
				filename = this.task.get('id') +'/'+ file.id +'.'+ extension,
				data = {
					name: file.name,
					filename: filename,
					size: file.size,
					type: file.type
				};
			var attachment = this.attachments.add(data);
			var view = new App.Views.CreateTaskCommentAttachmentView({ model: attachment, collection: this.attachments, file_id: file.id });
			filesList.append(view.render().el);
		}, this);
		up.start();
		this.$el.find('.btn').attr('disabled', true);
		this.$el.find('.attach-files').text('Uploading...');
		up.disableBrowse(true);
	},

	uploadError: function(up, err) {
		this.$el.find('.attach-files').text('Attach Files');
		alert(err.message);
	},

	beforeUpload: function(up, file) {
		// Hack to make unique_names work
		var file_name = file.name;
		var extension = file.name.split('.').pop();
		up.settings.multipart_params.key = this.task.get('id') +'/'+ file.id +'.'+ extension;
		up.settings.multipart_params.Filename = this.task.get('id') +'/'+ file.id +'.'+ extension;
	},

	uploadProgress: function(up, file) {
		this.$el.find('#'+ file.id +' .progress').text(file.percent +'%');
	},

	fileUploaded: function(up, file) {
		this.$el.find('#'+ file.id).addClass('uploaded');
	},

	uploadComplete: function(up, file) {
		this.$el.find('.btn').attr('disabled', false);
		this.$el.find('.attach-files').text('Attach Files');
		up.disableBrowse(false);
	},

	saveAttachments: function(comment) {
		var that = this;
		if(this.attachments.length){
			this.attachments.each(function(attachment){
				attachment.set('comment_id', comment.get('id'));
				attachment.save({ wait: true }).done(function(){
					comment.trigger('attachments-saved');
					that.attachments.remove(attachment);
				});
			});

		}
	},

	submit: function(e) {
		e.preventDefault();
	},

	formattingHelp: function(e) {
		e.preventDefault();
		var view = new App.Views.FormattingHelpModal();
		$('body').append(view.render().el);
	}

});

App.Views.TaskCommentAttachmentsView = Backbone.View.extend({

	tagName: 'ul',
	className: 'task-comment-attachments',

	initialize: function() {
		this.attachments = new App.Collections.TaskCommentAttachments();

		this.listenTo(this.attachments, 'add', this.addOne);
		this.listenTo(this.attachments, 'reset', this.addAll);

		this.attachments.fetch({ data: { comment_id: this.model.get('id') } });
	},

	addOne: function(model) {
		var view = new App.Views.TaskCommentAttachmentView({ model: model });
		this.$el.append(view.render().el);
	},

	addAll: function() {
		this.$el.html('');
		this.attachments.each(this.addOne, this);
	},

	refresh: function() {
		this.attachments.fetch({
			data: { comment_id: this.model.get('id') },
			remove: false
		});
	}

});

App.Views.TaskCommentAttachmentView = Backbone.View.extend({

	tagName: 'li',
	className: 'task-comment-attachment',
	template: App.Templates.TaskCommentAttachment,

	initialize: function() {
		this.listenTo(this.model, 'sync', this.render);
	},

	render: function() {
		var file_type = 'normal';
		if(_.contains(['image/gif','image/jpeg','image/png'], this.model.get('type'))){
			file_type = 'image';
		}

		var data = this.model.toJSON();
		data.file_type = file_type;
		data.extension = this.model.get('name').split('.').pop();
		this.$el.html(this.template(data));
		return this;
	},

});

App.Views.CreateTaskCommentAttachmentView = Backbone.View.extend({

	tagName: 'li',
	className: 'create-task-comment-attachment',
	template: App.Templates.TaskCommentAttachmentCreate,

	events: {
		'click .delete-attachment': 'deleteAttachment'
	},

	initialize: function(atts) {
		this.file_id = atts.file_id;

		this.listenTo(this.model, 'sync', this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.toJSON()));
		this.$el.attr('id', this.file_id);
		this.$el.addClass(this.model.get('name').split('.').pop());
		return this;
	},

	deleteAttachment: function(e) {
		e.preventDefault();
		this.collection.remove(this.model);
		this.remove();
	}

});

App.Views.FormattingHelpModal = Backbone.Modal.extend({

	className: 'formatting-help-modal',
	template: App.Templates.FormattingHelp,

	events: {
		'click .cancel': 'hideModal'
	},

	hideModal: function(e) {
		e.preventDefault();
		this.triggerCancel();
	}

});
