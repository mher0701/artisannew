App.Views.ActivityMenuView = Backbone.View.extend({

	tagName: 'li',
	className: 'activity dropit-trigger',
	template: App.Templates.ActivityMenu,

	events: {
		'click .icon-bell': 'openMenu'
	},

	initialize: function() {
		this.history = new App.Collections.History({ model: App.Models.History });

		_.bindAll(this, 'loaded');
		this.listenTo(this.history, 'add', this.addOne);
		this.listenTo(this.history, 'reset', this.addAll);

		$('.settings-nav .settings').before(this.render().el);
	},

	openMenu: function() {
		this.history.fetch({
			success: this.loaded,
			data: { limit: 5 }
		});
	},

	loaded: function() {
		if(this.history.length){
			this.$el.find('.loading').remove();
			this.$el.find('.view-activity').show();
		} else {
			this.$el.find('.loading a').text('No recent activity');
			this.$el.find('.view-activity').hide();
		}
	},

	addOne: function(model) {
		var view = new App.Views.ActivityItemView({ model: model });
		view.render().$el.insertBefore(this.$el.find('.view-activity'));
	},

	addAll: function() {
		this.$el.find('.item').remove();
		this.history.each(this.addOne, this);
	}

});

App.Views.ActivityItemView = Backbone.View.extend({

	tagName: 'li',
	className: 'item',
	template: App.Templates.ActivityItem,

	initialize: function() {
		this.model.set('content', JSON.parse(this.model.get('content')));

		this.listenTo(this.model, 'sync', this.render);
	},

	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}

});

App.Views.HistoryView = Backbone.View.extend({

	tagName: 'ul',
	id: 'activity',

	initialize: function(atts) {
		this.history = new App.Collections.History({ model: App.Models.History });
		this.$el.appendTo('#content');

		this.listenTo(this.history, 'add', this.addOne);
		this.listenTo(this.history, 'reset', this.addAll);

		this.history.fetch();
	},

	addOne: function(model) {
		var view = new App.Views.HistoryItemView({ model: model });
		this.$el.append(view.render().el);
	},

	addAll: function() {
		this.$el.html('');
		this.history.each(this.addOne, this);
	}

});

App.Views.HistoryItemView = Backbone.View.extend({

	tagName: 'li',
	className: 'item',
	template: App.Templates.HistoryItem,

	initialize: function() {
		this.model.set('content', JSON.parse(this.model.get('content')));

		this.listenTo(this.model, 'sync', this.render);
	},

	render: function(){
		this.$el.html(this.template(this.model.toJSON()));
		return this;
	}

});
