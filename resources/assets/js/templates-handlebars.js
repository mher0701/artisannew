this["App"] = this["App"] || {};
this["App"]["Templates"] = this["App"]["Templates"] || {};

this["App"]["Templates"]["ActivityMenu"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<a href=\"#\" class=\"icon-bell\" title=\"Activity\"></a>\n<ul class=\"dropit-submenu\">\n	<li class=\"loading\"><a href=\"#/activity\">Loading...</a></li>\n	<li class=\"view-activity\"><a href=\"#/activity\">View Recent Activity</a></li>\n</ul>\n";
  });

this["App"]["Templates"]["FormattingHelp"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Formatting Help</h3>\n</div>\n<div class=\"modal-content\">\n	<p>Artisan supports Markdown editing in comments. Here are some examples:</p>\n	<p><strong>Italics &amp; Bold</strong></p>\n	<table>\n		<tr>\n			<td style=\"width:50%\"><code>*This is italicized*</code></td>\n			<td><em>This is italicized</em></td>\n		</tr>\n		<tr>\n			<td><code>**This is bold**</code></td>\n			<td><strong>This is bold</strong></td>\n		</tr>\n	</table>\n	<p><strong>Links</strong></p>\n	<table>\n		<tr>\n			<td style=\"width:50%\"><code>[artisan](http://artisan.so)</code></td>\n			<td><a href=\"http://artisan.so\" target=\"_blank\">artisan</a></td>\n		</tr>\n	</table>\n	<p><strong>Lists</strong></p>\n	<table>\n		<tr>\n			<td style=\"width:50%\"><pre><code>* List item 1\n* List item 2\n* List item 3</code></pre></td>\n			<td><ul><li>List item 1</li><li>List item 2</li><li>List item 3</li></ul></td>\n		</tr>\n		<tr>\n			<td><pre><code>1. List item 1\n2. List item 2\n3. List item 3</code></pre></td>\n			<td><ol><li>List item 1</li><li>List item 2</li><li>List item 3</li></ol></td>\n		</tr>\n	</table>\n</div>\n<div class=\"modal-footer\">\n	<a href=\"#\" class=\"btn alt cancel\">Close</a>\n</div>\n";
  });

this["App"]["Templates"]["FreeTrialModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Start your <del>14</del> 60 day free trial</h3>\n</div>\n<div class=\"modal-content\">\n	<p>Once you start your free trial you will be able to create your own projects and\n	invite members to your team. At the end of the free trial you will need to upgrade to\n	a paid plan to continue using these features.</p>\n</div>\n<div class=\"modal-footer\">\n	<a href=\"#\" class=\"btn alt cancel\">Cancel</a>\n	<a href=\"/start-free-trial\" class=\"btn start\">Start Free Trial</a>\n</div>\n";
  });

this["App"]["Templates"]["MilestoneCreate"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"create-milestone-wrap\">\n	<a href=\"#\" class=\"create-milestone\">Create Milestone +</a>\n</div>\n<div class=\"milestone-form\"></div>\n";
  });

this["App"]["Templates"]["MilestoneEditModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Edit Milestone</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["MilestoneList"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"milestone-header\">\n	<a href=\"#/milestone/";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">\n		<span class=\"milestone-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n		<span class=\"milestone-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	</a>\n</div>\n<div class=\"milestone-body\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["Milestone"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<div class=\"milestone-header\">\n	<span class=\"breadcrumb\">\n		<a href=\"#/project/"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.project)),stack1 == null || stack1 === false ? stack1 : stack1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"project-breadcrumb\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.project)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n		<span class=\"sep\">/</span>\n	</span>\n	<span class=\"milestone-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"milestone-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<div class=\"milestone-meta\">\n		<span class=\"milestone-createdat\">Created on "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.created_at), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.created_at), options)))
    + "</span>\n		<a href=\"#\" class=\"icon-pencil milestone-edit\" title=\"Edit Milestone\"></a>\n		<a href=\"#\" class=\"icon-trash milestone-delete\" title=\"Delete Milestone\"></a>\n	</div>\n</div>\n<div class=\"milestone-body\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["NotFound"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<h1>Whoops!</h1>\n<p>It looks like this doesn't exist or has been deleted. Sorry about that.</p>\n";
  });

this["App"]["Templates"]["ProjectCreateModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Create Project</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["ProjectEditModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Edit Project</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["ProjectList"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, self=this, helperMissing=helpers.helperMissing, functionType="function", escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n				<li><img src=\"";
  stack1 = (helper = helpers.gravatar || (depth0 && depth0.gravatar),options={hash:{
    'size': ("30")
  },inverse:self.noop,fn:self.program(2, program2, data),data:data},helper ? helper.call(depth0, (depth0 && depth0.email), options) : helperMissing.call(depth0, "gravatar", (depth0 && depth0.email), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" title=\""
    + escapeExpression(((stack1 = (depth0 && depth0.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" alt=\"\"></li>\n			";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  buffer += "<a href=\"#/project/";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" class=\"project-link\">\n	<span class=\"project-name\" title=\"";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"project-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"project-meta\">\n		<ul class=\"users\">\n			";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.users), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		</ul>\n	</span>\n</a>\n<a href=\"#\" class=\"icon-pencil project-edit\" title=\"Edit Project\"></a>\n<a href=\"#\" class=\"icon-trash project-delete\" title=\"Delete Project\"></a>\n";
  return buffer;
  });

this["App"]["Templates"]["ProjectSelect"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, helper;
  buffer += "\n		<option value=\"";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</option>\n		";
  return buffer;
  }

  buffer += "<div class=\"styled-select\">\n	<select>\n		<option value=\"all\">All Projects</option>\n		";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.projects), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	</select>\n</div>\n";
  return buffer;
  });

this["App"]["Templates"]["Projects"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<h3 class=\"title your-projects-title\">Your Projects</h3>\n<ul class=\"your-projects\"></ul>\n\n<h3 class=\"title shared-projects-title\">Shared Projects</h3>\n<ul class=\"shared-projects\"></ul>\n";
  });

this["App"]["Templates"]["TaskCommentCreate"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

  buffer += "<div class=\"comment-author\">\n	<span class=\"avatar\"><img src=\"";
  stack1 = (helper = helpers.gravatar || (depth0 && depth0.gravatar),options={hash:{
    'size': ("40")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options) : helperMissing.call(depth0, "gravatar", ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" alt=\"\"></span>\n</div>\n<div class=\"comment-form\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["TaskComment"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "";
  return buffer;
  }

function program3(depth0,data) {
  
  var stack1;
  return escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1));
  }

function program5(depth0,data) {
  
  
  return "Unknown";
  }

  buffer += "<div class=\"comment-author\">\n	<span class=\"avatar\"><img src=\"";
  stack1 = (helper = helpers.gravatar || (depth0 && depth0.gravatar),options={hash:{
    'size': ("40")
  },inverse:self.noop,fn:self.program(1, program1, data),data:data},helper ? helper.call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options) : helperMissing.call(depth0, "gravatar", ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.email), options));
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" alt=\"\"></span>\n	<span class=\"name\">";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.user)),stack1 == null || stack1 === false ? stack1 : stack1.name), {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "</span><br>\n	<span class=\"date\">Posted "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{
    'format': ("relative")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.created_at), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.created_at), options)))
    + "</span>\n	<span class=\"comment-delete\">- <a href=\"#\" class=\"delete\">Delete</a></span>\n</div>\n<div class=\"comment-content\">";
  if (helper = helpers.content) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.content); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</div>\n<div class=\"comment-attachments\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["TaskEditModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Edit Task</h3>\n</div>\n<div class=\"modal-content\"></div>\n";
  });

this["App"]["Templates"]["TaskList"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<span class=\"task-category\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.category)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n	";
  return buffer;
  }

function program3(depth0,data) {
  
  var stack1;
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.phase), {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  }
function program4(depth0,data) {
  
  
  return "\n	<span class=\"sep\">/</span>\n	";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<span class=\"task-phase\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.phase)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n	";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n	<span class=\"task-assignedto\">("
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.assigned_user)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ")</span>\n	";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\n	<span class=\"task-duedate\">"
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options)))
    + "</span>\n	";
  return buffer;
  }

function program12(depth0,data) {
  
  
  return " checked=\"checked\"";
  }

  buffer += "<div class=\"pull-right\">\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.category), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.category), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.phase), {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.assigned_user), {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n	";
  stack1 = (helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options));
  stack1 = helpers['if'].call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>\n<input type=\"checkbox\" class=\"completed\"";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.completed), {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n<a href=\"#/task/";
  if (helper = helpers.id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">\n	<span class=\"task-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"task-description\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n</a>\n";
  return buffer;
  });

this["App"]["Templates"]["Task"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, helper, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<span class=\"task-category\"><strong>Category:</strong> "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.category)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n		";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<span class=\"task-phase\"><strong>Phase:</strong> "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.phase)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n		";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n		<span class=\"task-assignedto\"><strong>Assigned to:</strong> "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.assigned_user)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</span>\n		";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", helper, options;
  buffer += "\n		<span class=\"task-duedate\">Due date: "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options)))
    + "</span>\n		";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", stack1, helper, options;
  buffer += "\n		<span class=\"task-createdby\">Created by "
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.created_by)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " "
    + escapeExpression((helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{
    'format': ("relative")
  },data:data},helper ? helper.call(depth0, (depth0 && depth0.created_at), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.created_at), options)))
    + "</span>\n		";
  return buffer;
  }

  buffer += "<div class=\"task-header\">\n	<span class=\"breadcrumb\">\n		<a href=\"#/project/"
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.milestone)),stack1 == null || stack1 === false ? stack1 : stack1.project)),stack1 == null || stack1 === false ? stack1 : stack1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"project-breadcrumb\">"
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = (depth0 && depth0.milestone)),stack1 == null || stack1 === false ? stack1 : stack1.project)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n		<span class=\"sep\">/</span>\n		<a href=\"#/milestone/";
  if (helper = helpers.milestone_id) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.milestone_id); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\" class=\"milestone-breadcrumb\">"
    + escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.milestone)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n		<span class=\"sep\">/</span>\n	</span>\n	<span class=\"task-name\">";
  if (helper = helpers.name) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.name); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<span class=\"task-description\" title=\"";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (helper = helpers.description) { stack1 = helper.call(depth0, {hash:{},data:data}); }
  else { helper = (depth0 && depth0.description); stack1 = typeof helper === functionType ? helper.call(depth0, {hash:{},data:data}) : helper; }
  buffer += escapeExpression(stack1)
    + "</span>\n	<div class=\"task-meta\">\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.category), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.phase), {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.assigned_user), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = (helper = helpers.formatDate || (depth0 && depth0.formatDate),options={hash:{},data:data},helper ? helper.call(depth0, (depth0 && depth0.due_date), options) : helperMissing.call(depth0, "formatDate", (depth0 && depth0.due_date), options));
  stack1 = helpers['if'].call(depth0, stack1, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.created_by), {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n		<a href=\"#\" class=\"icon-tick task-complete\" title=\"Complete Task\"></a>\n		<a href=\"#\" class=\"icon-pencil task-edit\" title=\"Edit Task\"></a>\n		<a href=\"#\" class=\"icon-trash task-delete\" title=\"Delete Task\"></a>\n	</div>\n</div>\n<div class=\"task-body\"></div>\n";
  return buffer;
  });

this["App"]["Templates"]["UpgradeModal"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"modal-header\">\n	<h3 class=\"modal-title\">Time to Upgrade</h3>\n</div>\n<div class=\"modal-content\"></div>\n<div class=\"modal-footer\">\n	<a href=\"#\" class=\"btn alt cancel\">Cancel</a>\n	<a href=\"/billing\" class=\"btn upgrade\">Upgrade</a>\n</div>\n";
  });