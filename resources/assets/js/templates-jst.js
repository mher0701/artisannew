this["App"] = this["App"] || {};
this["App"]["Templates"] = this["App"]["Templates"] || {};

this["App"]["Templates"]["ActivityItem"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {


if(action == 'deleted'){
	print('<a href="#/activity">');
} else {
	if(type == 'comment'){
		print('<a href="#/task/'+ content.task_id +'">');
	} else {
		print('<a href="#/'+ type +'/'+ type_id +'">');
	}
}
;
__p += '\n<span class="user">' +
((__t = ( user ? user.name : 'Unknown' )) == null ? '' : __t) +
'</span>\n<span class="action">';

if(action == 'created' && type == 'comment'){
	print('commented')
} else {
	print(action +' the '+ type);
}
;
__p += '</span>\n';

if(type == 'comment'){
	print('<span class="object">&ldquo;'+ stripHTML(content.content.substring(0,10)) +'...&rdquo;</span>');
} else {
	print('<span class="object">'+ content.name +'</span>');
}
;
__p += '\n</a>\n';

}
return __p
};

this["App"]["Templates"]["HistoryItem"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
with (obj) {
__p += '<img src="' +
((__t = ( gravatar((user ? user.email : 'hi@artisan.so'), 30) )) == null ? '' : __t) +
'" alt="">\n<span class="user">' +
((__t = ( user ? user.name : 'Unknown' )) == null ? '' : __t) +
'</span>\n<span class="action">';

if(action == 'created' && type == 'comment'){
	print('commented')
} else {
	print(action +' the '+ type);
}
;
__p += '</span>\n';

if(type == 'comment'){
	if(action == 'deleted'){
		print('<span class="object">&ldquo;'+ stripHTML(content.content.substring(0,50)) +'...&rdquo;</span>');
	} else {
		print('&ldquo;<a href="#/task/'+ content.task_id +'" class="object">'+ stripHTML(content.content.substring(0,50)) +'...</a>&rdquo;');
	}
} else {
	if(action == 'deleted'){
		print('<span class="object">'+ content.name +'</span>');
	} else {
		print('<a href="#/'+ type +'/'+ type_id +'" class="object">'+ content.name +'</a>');
	}
}
;
__p += '\n<span class="time">' +
((__t = ( moment(created_at).fromNow() )) == null ? '' : __t) +
'</span>\n';

}
return __p
};

this["App"]["Templates"]["TaskCommentAttachmentCreate"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p +=
((__t = ( name )) == null ? '' : __t) +
' <span class="size">(' +
((__t = ( formatSize(size) )) == null ? '' : __t) +
')</span>\n<span class="progress"></span>\n<a href="#" class="icon-cross delete-attachment" title="Delete ' +
((__t = ( name )) == null ? '' : __t) +
'"></a>\n';

}
return __p
};

this["App"]["Templates"]["TaskCommentAttachment"] = function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<a href="//cdn.artisan.so/' +
((__t = ( filename )) == null ? '' : __t) +
'" target="_blank" title="Download ' +
((__t = ( name )) == null ? '' : __t) +
'" download="' +
((__t = ( name )) == null ? '' : __t) +
'">\n\t<div class="thumb' +
((__t = ( file_type != 'image' ? ' icon-file' : '' )) == null ? '' : __t) +
' ' +
((__t = ( extension )) == null ? '' : __t) +
'"' +
((__t = ( file_type == 'image' ? ' style="background-image:url(//cdn.artisan.so/'+ filename +')"' : '' )) == null ? '' : __t) +
'></div>\n\t<div class="name">' +
((__t = ( name )) == null ? '' : __t) +
'</div>\n\t<div class="meta">\n\t\t<span class="size">' +
((__t = ( formatSize(size) )) == null ? '' : __t) +
'</span>\n\t</div>\n</a>\n';

}
return __p
};