@extends('layout')

@section('content')

	<div id="signup" class="closed">
		<h1>Sign Up Closed</h1>
		<p>Unfortunaetly Artisan isn't quite ready yet. Pop in your email address below to get notified
		when we launch Artisan to the public.</p>
		<form action="http://dev7studios.createsend.com/t/r/s/tduidyi/" method="post">
		    <input id="fieldEmail" name="cm-tduidyi-tduidyi" type="email" required placeholder="Email Address" />
		    <button type="submit" class="btn">Get Notified</button>
		</form>

		<br><br>
		<a href="https://twitter.com/artisanapp" class="twitter-follow-button" data-show-count="false">Follow @artisanapp</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	</div>

@stop
