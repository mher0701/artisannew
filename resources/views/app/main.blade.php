@extends('layout')

@section('footer')

    @if (Auth::check())
        <script>
        var currentUser = {
                "id":"{{Auth::user()->id}}",
                "email":"{{Auth::user()->email}}",
                "name":"{{Auth::user()->name}}",
                "role":"{{Auth::user()->role}}",
                "status":"{{Auth::user()->status}}",
                "created_at":"{{Auth::user()->created_at}}",
                "updated_at":"{{Auth::user()->updated_at}}",
                "stripe_active":"{{Auth::user()->stripe_active}}",
                "stripe_id":"{{Auth::user()->stripe_id}}",
                "stripe_plan":"{{Auth::user()->stripe_plan}}",
                "last_four":"{{Auth::user()->last_four}}",
                "trial_ends_at":"{{Auth::user()->trial_ends_at}}",
                "subscription_ends_at":"{{Auth::user()->subscription_ends_at}}",
                "used_free_trial":"{{Auth::user()->used_free_trial}}",
            }
        currentUser.apiKey = "{{ Auth::user()->getAuthApiKey() }}";
        var awsData = {
            bucket: "artisanapp",
            accessKey: "AKIAI4GVKFONIUZCWSDA",
            policy: "{{ $policy = base64_encode(json_encode(array(
            	'expiration' => date('Y-m-d\TH:i:s.000\Z', strtotime('+1 day')),
            	'conditions' => array(
            		array('bucket' => 'artisanapp'),
            		array('acl' => 'public-read'),
            		array('starts-with', '$key', ''),
            		array('starts-with', '$Content-Type', ''),
            		// Plupload internally adds name field, so we need to mention it here
            		array('starts-with', '$name', ''),
            		// One more field to take into account: Filename - gets silently sent by FileReference.upload() in Flash
            		array('starts-with', '$Filename', ''),
            	)
            ))) }}",
            signature: "{{ base64_encode(hash_hmac("sha1", $policy, 'Cm7VO41aiEQnwLnMpCSa1jO9ubtiCWt7yJN3mg9p', true)) }}"
        };
        </script>
        <script src="{{ url('bower_components/underscore/underscore.js') }}"></script>
        <script src="{{ url('bower_components/backbone/backbone.js') }}"></script>
        <script src="{{ url('bower_components/handlebars/handlebars.js') }}"></script>
        <script src="{{ url('bower_components/Backbone.Handlebars/lib/backbone_handlebars.js') }}"></script>
        <script src="{{ url('bower_components/backbone-forms/distribution/backbone-forms.min.js') }}"></script>
        <script src="{{ url('scripts/bootstrap-modal.js') }}"></script>
        <script src="{{ url('bower_components/backbone-forms/distribution/adapters/backbone.bootstrap-modal.js') }}"></script>
        <script src="{{ url('bower_components/backbone-forms/distribution/editors/list.js') }}"></script>
        <script src="{{ url('bower_components/backbone-modal/backbone.modal.js') }}"></script>
        <script src="{{ url('bower_components/js-md5/js/md5.min.js') }}"></script>
        <script src="{{ url('bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ url('bower_components/plupload/js/plupload.full.min.js') }}"></script>
        @if (App::environment('local'))
        <script src="{{ url('scripts/app.compiled.js') }}"></script>
        @else
        <script src="{{ url('scripts/app.min.js') }}"></script>
        @endif

    @endif

@stop
