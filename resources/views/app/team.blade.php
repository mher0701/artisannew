@extends('layout')

@section('header')

    <a href="/" class="project-select">All Projects</a>

@stop

@section('content')

    <div id="team">
        <h1>Manage Team</h1>
        @if ($current_project && $projects->count())
            <div class="styled-select">
                <select id="project-select">
                @foreach ($projects as $project)
                    <option value="{{ $project->id }}"@if ($project->id == $current_project->id) selected="selected"@endif>{{ $project->name }}</option>
                @endforeach
                </select>
            </div>

            <table class="users-access">
                <thead>
                    <tr>
                        <th>User</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($current_project->users))
                        @foreach ($current_project->users as $user)
                            <tr id="user-{{ $user->id }}">
                                <td>
                                    <img src="{{ Gravatar::get($user->email, 30) }}" width="30px" alt="" class="avatar">
                                    {{ $user->name }} ({{ $user->email }})
                                    @if ($user->status != 'active')
                                    <span class="status {{ $user->status }}">{{ ucfirst($user->status) }}</span>
                                    @endif
                                </td>
                                <td class="action">
                                    @if ($user->id == $current_project->owner)
                                    <span class="owner">Owner</span>
                                    @else
                                    <a href="#" class="icon-trash remove-user" data-project-id="{{ $current_project->id }}" data-user-id="{{ $user->id }}" title="Remove {{ $user->name }} from {{ $current_project->name }}" data-confirm="Are you sure you want to remove {{ $user->name }} from {{ $current_project->name }}?"></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                    <tr><td colspan="2" class="no-users">No users yet...</td></tr>
                    @endif
                </tbody>
            </table>

            @if ($can_add_users)
            <h3>Add User to {{ $current_project->name }}</h3>
            {{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
            {{ Form::open(array('url' => 'team/add-user', 'id' => 'add-user-form')) }}
                {{ Form::hidden('project_id', $current_project->id) }}
                {{ Form::label('name', 'Name') }}
                {{ $errors->first('name') ? '<span class="error">'. $errors->first('name') .'</span>' : '' }}<br>
                {{ Form::text('name', Input::old('name')) }}
                {{ Form::label('email', 'Email Address') }}
                {{ $errors->first('email') ? '<span class="error">'. $errors->first('email') .'</span>' : '' }}<br>
                {{ Form::text('email', Input::old('email')) }}
                {{ Form::submit('Invite User', array('class' => 'btn')) }}
            {{ Form::close() }}
            @else
            <p class="upgrade">You have used up your quota of users for this team. <a href="/billing">Upgrade to add more users &rarr;</a></p>
            @endif
        @else
            <p class="no-projects">You currently don't own any projects. Only project owners can manage a project's team.</p>
        @endif
    </div>

@stop

@section('footer')

    <script src="{{ url('scripts/team.js') }}"></script>

@stop
