@extends('layout')

@section('header')

	<a href="/" class="project-select">All Projects</a>

@stop

@section('content')
	<div id="billing">
		<h1>Billing</h1>
		@if (Auth::user()->onGracePeriod())
		<p class="error">You have cancelled your subscription and it will expire on {{ date('j F Y', strtotime(Auth::user()->subscription_ends_at)) }}.</p>
		@endif
		{{ Session::get('message') ? '<p class="success">'. Session::get('message') .'</p>' : '' }}
		<div class="pricing-table">
			<div class="plan startup">
				<h2>Startup</h2>
				<ul>
					<li><strong>5</strong> Projects</li>
					<li><strong>3</strong> Users</li>
					<li>Unlimited file storage</li>
					<li>Email support</li>
				</ul>
				<span class="price">$29<span class="timeframe">/month</span></span>
				@if (Auth::user()->stripe_plan == 'artisan_startup' && !Auth::user()->onGracePeriod())
				<span class="btn alt">Current Plan</span>
				@else
				<a href="/billing/upgrade/startup" class="btn">Upgrade &rarr;</a>
				@endif
			</div>
			<div class="plan agency">
				<h2>Agency</h2>
				<ul>
					<li><strong>15</strong> Projects</li>
					<li><strong>9</strong> Users</li>
					<li>Unlimited file storage</li>
					<li>Email support</li>
				</ul>
				<span class="price">$39<span class="timeframe">/month</span></span>
				@if (Auth::user()->stripe_plan == 'artisan_agency' && !Auth::user()->onGracePeriod())
				<span class="btn alt">Current Plan</span>
				@else
				<a href="/billing/upgrade/agency" class="btn">Upgrade &rarr;</a>
				@endif
			</div>
			<div class="plan enterprise">
				<h2>Business</h2>
				<ul>
					<li><strong>50</strong> Projects</li>
					<li><strong>18</strong> Users</li>
					<li>Unlimited file storage</li>
					<li>Email support</li>
				</ul>
				<span class="price">$99<span class="timeframe">/month</span></span>
				@if (Auth::user()->stripe_plan == 'artisan_business' && !Auth::user()->onGracePeriod())
				<span class="btn alt">Current Plan</span>
				@else
				<a href="/billing/upgrade/business" class="btn">Upgrade &rarr;</a>
				@endif
			</div>
			<p class="risk">No commitment. Change plans or cancel any time.<br>
			Need more projects, users or a higher level of support? <a href="mailto:support@artisan.so">Send us an email</a></p>
			@if (Auth::user()->stripe_plan != '' && !Auth::user()->onGracePeriod())
			<a href="/billing/edit" class="btn alt edit-billing">Cancel your subscription</a>
			@endif
			@if (Auth::user()->stripe_plan == '' && !Auth::user()->onTrial())
			<a href="/downgrade-to-team" class="btn alt downgrade-team">Downgrade to Team Member</a>
			@endif
		</div>

		<div class="faq">
			<div class="pull-left">
				<h3>What types of payment do you accept?</h3>
				<p>We accept all major credit cards for every plan. We do not accept PayPal payments.</p>

				<h3>What happens after my free trial?</h3>
				<p>You can upgrade to any plan any time during your free trial. However, if you hit the end of your free trial without choosing a plan, your account will be put on hold (your data will all stay safe for 30 days) until you choose a plan (or cancel).</p>

				<h3>Do I have to sign a long-term contract?</h3>
				<p>No. Monthly plans are just pay as you go, and you can cancel any time without penalty.</p>
			</div>
			<div class="pull-right">
				<h3>Can I change my plan at any time?</h3>
				<p>Yep. Simply click the “Upgrade” button above for the plan you wish to change to.</p>

				<h3>Can I get a refund?</h3>
				<p>If you decide to cancel your account, you won't be charged again, but you are responsible for whatever charges have already been incurred for the current billing period.</p>
			</div>
		</div>

		@if (!empty($invoices))
		<div class="billing-history">
			<h2>Billing History</h2>
			<ul>
				@foreach ($invoices as $invoice)
				<li><a href="/billing/invoice/{{ $invoice->id }}">
					{{ $invoice->dateString() }} &mdash; {{ $invoice->dollars() }}
				</a></li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>

@stop
