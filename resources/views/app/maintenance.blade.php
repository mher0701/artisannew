@extends('layout')

@section('content')

	<div id="maintenance">
		<h1>We'll be right back</h1>
		Apologies but Artisan is down for maintenance at the moment. Usually this will only last for
		30 seconds or so. If you see this page for more than 5 minutes please <a href="mailto:support@artisan.so">let us know</a>.
	</div>

@stop
