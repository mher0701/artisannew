@extends('layout')

@section('header')

    <a href="/" class="project-select">All Projects</a>

@stop

@section('content')

    <div id="account">
        <h1>Manage Account</h1>
        <img src="{{ Gravatar::get($user->email, 100) }}" alt="" class="avatar">

        @if (Session::get('error'))
            <p class="error">{{ Session::get('error') }}</p>
        @elseif (Session::get('status'))
            <p class="success">{{ Session::get('status') }}</p>
        @endif

        {{ Form::model($user, ['action' => 'AccountController@postInfo']) }}
            {{ Form::label('name', 'Name') }}

            @if ($errors->first('name'))
                <span class="error">{{ $errors->first('name') }}</span>
            @endif
            <br>
            {{ Form::text('name') }}<br>
            {{ Form::label('email', 'Email Address') }}

            @if ($errors->first('email'))
                <span class="error">{{ $errors->first('email') }}</span>
            @endif
            <br>
            {{ Form::text('email') }}<br>
            {{ Form::submit('Save', array('class' => 'btn')) }}
        {{ Form::close() }}

        {{ Form::model($user, ['action' => 'AccountController@postPassword']) }}
            {{ Form::label('password', 'New Password') }}

            @if ($errors->first('password'))
                <span class="error">{{ $errors->first('password') }}</span>
            @endif
            <br>
            {{ Form::password('password') }}<br>
            {{ Form::label('password_confirmation', 'Confirm Password') }}

            @if ($errors->first('password_confirmation'))
                <span class="error">{{ $errors->first('password_confirmation') }}</span>
            @endif
            <br>
            {{ Form::password('password_confirmation') }}<br>
            {{ Form::submit('Save', array('class' => 'btn')) }}
        {{ Form::close() }}
    </div>

@stop