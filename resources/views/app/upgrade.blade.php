@extends('layout')

@section('header')

	<a href="/" class="project-select">All Projects</a>

@stop

@section('content')

	<div id="upgrade">
		@if (Auth::user()->stripe_plan != '')
		<h1>Switch to the<br>{{ ucfirst($plan) }} plan</h1>
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
		<p>You are about to switch to the {{ ucfirst($plan) }} plan.</p>
		{{ Form::open(array('url' => 'billing/upgrade/'. $plan, 'id' => 'upgrade-form')) }}
			<a href="/billing" class="return">Return to Billing</a>
			{{ Form::submit('Switch Plan', array('class' => 'btn')) }}
		{{ Form::close() }}
		@else
		<h1>Upgrade to the {{ ucfirst($plan) }} plan</h1>
		<p class="encryption">This page uses 128-bit encryption</p>
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
		{{ Form::open(array('url' => 'billing/upgrade/'. $plan, 'id' => 'upgrade-form')) }}
			{{ Form::label('Card Number') }}
			<input type="text" size="20" data-stripe="number"><br>
			{{ Form::label('CVC') }}
			<input type="text" size="4" data-stripe="cvc"><br>
			{{ Form::label('Expiration Month (MM)') }}
			<input type="text" size="2" data-stripe="exp-month"><br>
			{{ Form::label('Expiration Year (YYYY)') }}
			<input type="text" size="4" data-stripe="exp-year"><br>
			{{ Form::submit('Upgrade My Account', array('class' => 'btn')) }}
		{{ Form::close() }}
		@endif
	</div>

@stop

@section('footer')

	@if (Auth::user()->stripe_plan == '')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
	Stripe.setPublishableKey('{{ App::environment('production') ? 'pk_test_SMk3YnfKemqeS3HZVHf7oU4k' : 'pk_test_SMk3YnfKemqeS3HZVHf7oU4k' }}');
	</script>
	<script src="{{ url('scripts/upgrade.js') }}"></script>
	@endif

@stop
