@extends('layout')

@section('header')

	<a href="/signup" class="project-select">Don't have an account? Sign Up</a>

@stop

@section('content')

	<div id="login">
		<h1>Login</h1>
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
		{{ Form::open(array('url' => 'login')) }}
			{{ Form::label('email', 'Email Address') }}
			{{ $errors->first('email') ? '<span class="error">'. $errors->first('email') .'</span>' : '' }}<br>
			{{ Form::text('email', Input::old('email')) }}
			{{ Form::label('password', 'Password') }}
			{{ $errors->first('password') ? '<span class="error">'. $errors->first('password') .'</span>' : '' }}<br>
			{{ Form::password('password') }}
			{{ Form::submit('Login', array('class' => 'btn')) }}
			{{ link_to('password/remind', 'Forgot password?', array('class' => 'forgot-password')) }}
		{{ Form::close() }}
	</div>

@stop
