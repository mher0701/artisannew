@extends('layout')

@section('header')

	<a href="/login" class="project-select">Already got an account? Login</a>

@stop

@section('content')

	<div id="signup">
		<h1>Get started in less than a minute&hellip;</h1>
		<p class="intro">No commitment. Cancel any time.</p>
		<p class="encryption">This page uses 128-bit encryption</p>
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
		{{ Form::open(array('url' => 'signup')) }}
			{{ Form::label('name', 'Name') }}

			@if($errors->first('name'))
				<span class="error">{{ $errors->first('name')}}</span>
			@endif
			<br>

			{{ Form::text('name', Input::old('name')) }}
			{{ Form::label('email', 'Email Address') }}

			@if($errors->first('email'))
				<span class="error">{{ $errors->first('email')}}</span>
			@endif
			<br>

			{{ Form::text('email', Input::old('email')) }}
			{{ Form::label('password', 'Password') }}
			@if($errors->first('password'))
				<span class="error">{{ $errors->first('password')}}</span>
			@endif
			<br>

			{{ Form::password('password') }}
			{{ Form::label('password_confirmation', 'Confirm Password') }}

			@if($errors->first('password_confirmation'))
				<span class="error">{{ $errors->first('password_confirmation')}}</span>
			@endif
			<br>

			{{ Form::password('password_confirmation') }}<br>
			<p class="terms">By signing up you agree to our <a href="/terms" target="_blank">T's &amp; C's</a></p>
			{{ Form::submit('Start my 14 day free trial', array('class' => 'btn')) }}
		{{ Form::close() }}
	</div>

@stop
