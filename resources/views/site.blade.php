<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@if (isset($page_title) && $page_title)
	{{{ $page_title }}} - Artisan
	@else
	Artisan - Project Management for Software Teams
	@endif</title>

	@if (isset($page_description) && $page_description)
	<meta name="description" content="{{{ $page_description }}}">
	@endif
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="{{ url('favicon.ico') }}" />

	<link rel="stylesheet" href="{{ url('styles/site.css') }}">

	<!-- <script type="text/javascript" src="//use.typekit.net/inm0gtw.js"></script> -->
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	@if (App::environment('production'))
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-3892196-62', 'artisan.so');
	ga('send', 'pageview');
	</script>
	@endif
</head>
<body>

	<header class="header">
		<div class="header-content">
			<a href="/" class="logo">Artisan</a>
			<ul class="nav">
				<li class="pricing"><a href="/pricing">Pricing</a></li>
				<li class="login"><a href="/login">Login</a></li>
				<li class="signup"><a href="/signup">Sign Up</a></li>
			</ul>
			@yield('header')
		</div>
	</header>

	<section class="content">
		@yield('content')
	</section>

	<footer class="footer">
	    <ul class="nav">
            <li><a href="/">Home</a></li>
			<li><a href="/pricing">Pricing</a></li>
			<li><a href="/terms">Terms</a></li>
			<li><a href="/privacy">Privacy</a></li>
			<li><a href="http://blog.artisan.so">Blog</a></li>
			<li><a href="mailto:support@artisan.so">Support</a></li>
        </ul>
		@yield('footer')
        <div class="pull-right">
			<span class="copyright">Copyright &copy; {{ date('Y') }}. All rights reserved.</span>
            <a href="https://twitter.com/artisanapp" class="twitter-follow-button" data-show-count="false">Follow @artisanapp</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        </div>
	</footer>

</body>
</html>
