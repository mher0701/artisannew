<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi {{ $user->name }},<br>
			<br>
			Thanks for signing up to Artisan! Here is a reminder of your login details:<br>
			<br>
			Login: {{ URL::to('login') }}<br>
			Email: {{ $user->email }}<br>
			<br>
			If you ever need any help just reply to this email.<br>
			<br>
			Thanks,<br>
			Team Artisan
		</div>
	</body>
</html>
