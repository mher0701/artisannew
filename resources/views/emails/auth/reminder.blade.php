<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			You are recieving this email because you have requested to reset your password from the form at
			{{ URL::to('password/remind') }}<br>
			<br>
			To reset your password, complete this form: {{ URL::to('password/reset', array($token)) }}<br>
			<br>
			If you did not request a password reset please ignore this email. No further action is required.
		</div>
	</body>
</html>
