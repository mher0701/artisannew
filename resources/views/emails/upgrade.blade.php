<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi {{ $user->name }},<br>
			<br>
			You have successfully upgraded to the <strong>{{ ucfirst($plan) }}</strong> plan on Artisan.
			You will now be charged monthly until you cancel your subscription. If you ever need any help
			just reply to this email.<br>
			<br>
			Thanks,<br>
			Team Artisan
		</div>
	</body>
</html>
