<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi {{ $user->name }},<br>
			<br>
			{{ $invited_by->name }} has invited you to join Artisan. Please visit the following URL
			to complete your signup process:<br>
			<br>
			{{ link_to('signup/invite/'. $token) }}<br>
			<br>
			If you need any help getting started just reply to this email.<br>
			<br>
			Thanks,<br>
			Team Artisan
		</div>
	</body>
</html>
