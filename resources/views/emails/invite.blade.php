<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi {{ $user->name }},<br>
			<br>
			{{ $invited_by->name }} has invited you to {{ $project->name }} on Artisan.<br>
			<br>
			Login to get started: {{ link_to('login') }}<br>
			<br>
			If you need any help just reply to this email.<br>
			<br>
			Thanks,<br>
			Team Artisan
		</div>
	</body>
</html>
