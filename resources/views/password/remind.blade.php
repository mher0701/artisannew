@extends('layout')

@section('content')

	<div id="forgot-password">
		<h1>Forgot Password</h1>
		@if(Session::get('error'))
			<p class="error"> {{Session::get('error')}}</p>
		@elseif(Session::get('status'))	
			<p class="success"> {{Session::get('success')}}</p>
		@endif
		{{ Form::open(array('action' => 'RemindersController@postRemind')) }}
			{{ Form::label('email', 'Email Address') }}<br>
			{{ Form::text('email', Input::old('email')) }}<br>
			{{ Form::submit('Send Reminder', array('class' => 'btn')) }}
			{{ link_to('login', 'Back to login', array('class' => 'back-to-login')) }}
		{{ Form::close() }}
	</div>

@stop
