@extends('layout')

@section('content')

	<div id="reset-password">
		<h1>Reset Password</h1>
		@if(Session::get('error'))
			<p class="error"> {{Session::get('error')}}</p>
		@endif
		{{ Form::open(array('action' => 'RemindersController@postReset')) }}
			{{ Form::hidden('token', $token) }}
			{{ Form::label('email', 'Email Address') }}<br>
			{{ Form::text('email', Input::old('email')) }}<br>
			{{ Form::label('password', 'Password') }}
			{{ Form::password('password') }}
			{{ Form::label('password_confirmation', 'Confirm Password') }}
			{{ Form::password('password_confirmation') }}
			{{ Form::submit('Reset Password', array('class' => 'btn')) }}
		{{ Form::close() }}
	</div>

@stop
