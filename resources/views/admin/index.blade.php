@extends('layout')

@section('content')

	<div id="admin">
		<h1>Admin</h1>
		<div id="users-table"></div>
		<a href="#" id="add-user">Add User +</a>
	</div>

@stop

@section('footer')

	<link rel="stylesheet" href="{{ url('bower_components/backgrid/lib/backgrid.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ url('bower_components/backgrid-paginator/backgrid-paginator.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ url('bower_components/backgrid-filter/backgrid-filter.css') }}" type="text/css">
	<script>
	var currentUser = {
                "id":"{{Auth::user()->id}}",
                "email":"{{Auth::user()->email}}",
                "name":"{{Auth::user()->name}}",
                "role":"{{Auth::user()->role}}",
                "status":"{{Auth::user()->status}}",
                "created_at":"{{Auth::user()->created_at}}",
                "updated_at":"{{Auth::user()->updated_at}}",
                "stripe_active":"{{Auth::user()->stripe_active}}",
                "stripe_id":"{{Auth::user()->stripe_id}}",
                "stripe_plan":"{{Auth::user()->stripe_plan}}",
                "last_four":"{{Auth::user()->last_four}}",
                "trial_ends_at":"{{Auth::user()->trial_ends_at}}",
                "subscription_ends_at":"{{Auth::user()->subscription_ends_at}}",
                "used_free_trial":"{{Auth::user()->used_free_trial}}",
            };
	currentUser.apiKey = "{{ Auth::user()->getAuthApiKey() }}";
	</script>
	<script src="{{ url('bower_components/underscore/underscore.js') }}"></script>
	<script src="{{ url('bower_components/backbone/backbone.js') }}"></script>
	<script src="{{ url('bower_components/handlebars/handlebars.js') }}"></script>
	<script src="{{ url('bower_components/Backbone.Handlebars/lib/backbone_handlebars.js') }}"></script>
	<script src="{{ url('bower_components/backbone-forms/distribution/backbone-forms.min.js') }}"></script>
	<script src="{{ url('bower_components/backbone-modal/backbone.modal.js') }}"></script>
	<script src="{{ url('bower_components/backgrid/lib/backgrid.js') }}"></script>
	<script src="{{ url('bower_components/backbone-pageable/lib/backbone-pageable.js') }}"></script>
	<script src="{{ url('bower_components/backgrid-paginator/backgrid-paginator.js') }}"></script>
	<script src="{{ url('bower_components/backgrid-filter/backgrid-filter.js') }}"></script>
	<script src="{{ url('scripts/setup.min.js') }}"></script>
	<script src="{{ url('scripts/admin.js') }}"></script>

@stop
