@extends('site')

@section('content')

	<div id="privacy">
		<h1>Privacy Policy</h1>
		<p>At Artisan, we collect and manage user data according to the following Privacy Policy. This document is part of Artisan's <a href="/terms">Terms &amp; Conditions</a>, and by using Artisan.so (the "Website"), you agree to the terms of this Privacy Policy and the Terms of Use. Please read the Terms of Use in their entirety, and refer to those for definitions and contacts.</p>

		<h3>Data Collected</h3>
		<p>We collect anonymous data from every visitor of the Website to monitor traffic and fix bugs. For example, we collect information like web requests, the data sent in response to such requests, the Internet Protocol address, the browser type, the browser language, and a timestamp for the request.</p>
		<p>We ask you to log in and provide certain personal information (such as your name and email address) in order to be able to create and save your portfolio and the assets associated with it. In order to enable these or any other login based features, we use cookies to store session information for your convenience. You can block or delete cookies and still be able to use Artisan, although if you do you will then be asked for your username and password every time you log in to the Website. In order to take advantage of certain features of the Website, you may also choose to provide us with other personal information, but your decision to utilise these features and provide such data will always be voluntary.</p>
		<p>You are able to view, change and remove your data associated with your portfolio. Should you choose to delete your account, we will automatically delete all data.</p>
		<p>Minors and children should not use Artisan. By using the Website, you represent that you have the legal capacity to enter into a binding agreement.</p>

		<h3>Cookies</h3>
		<p>A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server. This enables the web server to identify and track the web browser.</p>
		<p>Currently we operate an "implied consent" policy which means that we assume you are happy with this usage. If you are not happy, then you should not use this Website.</p>
		<p>We may use both "session" cookies and "persistent" cookies on the website. Session cookies will be deleted from your computer when you close your browser. Persistent cookies will remain stored on your computer until deleted, or until they reach a specified expiry date.</p>
		<p>We will use the session cookies to: keep track of you whilst you navigate the website; prevent fraud and increase website security. We will use the persistent cookies to: enable our website to recognise you when you visit and keep track of your preferences in relation to your use of our website.</p>

		<h3>Use of the Data</h3>
		<p>We only use your personal information to provide you the Artisan services or to communicate with you about the services or the Website.</p>
		<p>We may use your personal information to:</p>
		<ul>
			<li>administer the website;</li>
			<li>improve your browsing experience by personalising the website;</li>
			<li>enable your use of the services available on the website;</li>
			<li>send statements and invoices to you, and collect payments from you;</li>
			<li>send you general (non-marketing) commercial communications;</li>
			<li>send you email notifications which you have specifically requested;</li>
			<li>deal with enquiries and complaints made by or about you relating to the website;</li>
			<li>keep the website secure and prevent fraud;</li>
			<li>verify compliance with the terms and conditions governing the use of the website.</li>
		</ul>
		<p>We employ industry standard techniques to protect against unauthorised access of data about you that we store, including personal information.</p>
		<p>We do not share personal information you have provided to us without your consent, unless:</p>
		<ul>
			<li>doing so is appropriate to carry out your own request;</li>
			<li>we believe it's needed to enforce our Terms of Use, or that is legally required;</li>
			<li>we believe it's needed to detect, prevent or address fraud, security or technical issues;</li>
			<li>otherwise protect our property, legal rights, or that of others.</li>
		</ul>
		<p>We may contact you, by email or other means. For example, we may send you promotional emails relating to Artisan or other third parties we feels you would be interested in, or communicate with you about your use of the Artisan website. We may also use technology to alert us via a confirmation email when you open an email from us. If you do not want to receive email from Artisan, please opt out of receiving emails at the bottom of any Artisan emails or by editing your profile preferences.</p>

		<h3>Sharing of Data</h3>
		<p>We don't share your personal information with third parties. Only aggregated, anonymised data is periodically transmitted to external services to help us improve the Artisan Website and service. We currently use Google Analytics (traffic analysis, SEO optimisation), Campaign Monitor (mailing list management), Mailgun (transactional email delivery) and Help Scout (email support). We listed below what data these third parties extract exactly. Feel free to check out their own Privacy Policies to find out more.</p>
		<ol>
			<li>Google Analytics: anonymous (ad serving domains, browser type, demographics, language settings, page views, time/date), pseudonymous (IP address)</li>
			<li>Campaign Monitor: email address</li>
			<li>Mailgun: email address</li>
			<li>Help Scout: anonymous</li>
		</ol>
		<p>We employ and contract with people and other entities that perform certain tasks on our behalf and who are under our control (our "Agents"). We may need to share personal information with our Agents in order to provide products or services to you. Unless we tell you differently, our Agents do not have any right to use Personal Information or other information we share with them beyond what is necessary to assist us. You hereby consent to our sharing of Personal Information with our Agents.</p>
		<p>We may choose to buy or sell assets. In these types of transactions, user information is typically one of the transferred business assets. Moreover, if we, or substantially all of our assets, were acquired, or if we go out of business or enter bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of us or our assets may continue to use your personal information as set forth in this policy.</p>

		<h3>International Data Transfers</h3>
		<p>Information that we collect may be stored and processed in and transferred between any of the countries in which we operate in order to enable us to use the information in accordance with this privacy policy.</p>
		<p>Information which you provide may be transferred to countries which do not have data protection laws equivalent to those in force in the European Economic Area.</p>
		<p>In addition, personal information that you submit for publication on the website will be published on the internet and may be available, via the internet, around the world. We cannot prevent the use or misuse of such information by others.</p>

		<h3>Security of your personal information</h3>
		<p>We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.</p>
		<p>We will store all the personal information you provide on our secure and firewall-protected servers.</p>
		<p>You acknowledge that the transmission of information over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.</p>
		<p>You are responsible for keeping your password and other login details confidential. We will not ask you for your password (except when you log in to the website).</p>

		<h3>Your rights</h3>
		<p>You may instruct us to provide you with any personal information we hold about you. Provision of such information will be subject to:</p>
		<ol>
			<li>the payment of a fee (currently fixed at GBP 10); and</li>
			<li>the supply of appropriate evidence of your identity (for this purpose, we will usually accept a photocopy of your passport certified by a solicitor or bank plus an original copy of a utility bill showing your current address).</li>
		</ol>

		<h3>Changes to the Privacy Policy</h3>
		<p>We may amend this Privacy Policy from time to time. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used. If we make major changes in the way we collect or use information, we will notify you by posting an announcement on the Website or sending you an email. A user is bound by any changes to the Privacy Policy when he or she uses the Services after such changes have been first posted.</p>
		<p>Should you have any question or concern, <a href="mailto:support@artisan.so">you can contact us by email</a>.</p>
	</div>

@stop
