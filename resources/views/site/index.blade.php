@extends('site')

@section('header')

	<div class="page-title">
		<h1>Your todo's, bugs, issues and features in one place</h1>
	    <h2>Stop using multiple apps or your inbox to manage your software projects, use Artisan instead</h2>
		<a href="/signup" class="btn">Get Started &rarr;</a>
		<br><span class="risk">No commitments. Cancel anytime.</span>
	</div>

@stop

@section('content')

	<div id="home">
		<div class="features">
			<div class="feature">
				<img src="{{ url('images/feature1.png') }}" alt="Creating a task in Artisan" />
				<h3>Supercharge your Todo's</h3>
				<p>Artisan tasks have the simplicity you would expect from a normal todo list combined
				with the power and flexability you would expect from a fully fledged bug or issue tracker.</p>
				<p>Tasks can be completed with a single click but can also be assigned customizable categories
				and phases, assigned to specfic team members and given a due date.</p>
			</div>
			<div class="feature alt">
				<img src="{{ url('images/feature2.png') }}" alt="Milestones in Artisan" />
				<h3>Project Status at a Glance</h3>
				<p>Artisan's structure is simple, everything is broken down into projects, milestones and tasks.
				The UI is designed in such a way that it is easy to get an overview of your project at a glance.</p>
				<p>Having a proper workflow is important and Artisan tries to meet the balance of giving just
				the right amount of structure while staying as flexable for your team as possible.</p>
			</div>
			<div class="feature">
				<img src="{{ url('images/feature3.png') }}" alt="Discussion of a task in Artisan" />
				<h3>Everything in One Place</h3>
				<p>Many teams use multiple apps to create their software and track todo's, bugs, issues and
				features. Artisan allows you to track and collaborate on all of these things with ease.</p>
				<p>Artisan integrates with other popular apps that teams use to create software so that
				everything can be pulled together and managed from a single location.</p>
			</div>
		</div>

		<div class="cta">
			<p>Give Artisan a try &mdash; It's free for 14 days. No credit card required.</p>
			<a href="/signup" class="btn">Get Started &rarr;</a>
		</div>
	</div>

@stop
