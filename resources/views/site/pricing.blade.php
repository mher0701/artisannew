@extends('site')

@section('content')

	<div id="pricing">
		<h1>Try Artisan free for 14 days.<br>
		No commitment. Change plans or cancel any time.</h1>
		<div class="pricing-table">
			<div class="plan startup">
				<h2>Startup</h2>
				<ul>
					<li><strong>5</strong> Projects</li>
					<li><strong>3</strong> Users</li>
					<li>Unlimited file storage</li>
					<li>Email support</li>
				</ul>
				<span class="price">$29<span class="timeframe">/month</span></span>
			</div>
			<div class="plan agency">
				<h2>Agency</h2>
				<ul>
					<li><strong>15</strong> Projects</li>
					<li><strong>9</strong> Users</li>
					<li>Unlimited file storage</li>
					<li>Email support</li>
				</ul>
				<span class="price">$39<span class="timeframe">/month</span></span>
			</div>
			<div class="plan business">
				<h2>Business</h2>
				<ul>
					<li><strong>50</strong> Projects</li>
					<li><strong>18</strong> Users</li>
					<li>Unlimited file storage</li>
					<li>Email support</li>
				</ul>
				<span class="price">$99<span class="timeframe">/month</span></span>
			</div>
			<p class="risk">Need more projects, users or a higher level of support? <a href="mailto:support@artisan.so">Send us an email</a></p>
			<a href="/signup" class="btn">Start my free trial &rarr;</a>
		</div>

		<div class="faq">
			<div class="pull-left">
				<h3>What types of payment do you accept?</h3>
				<p>We accept all major credit cards for every plan. We do not accept PayPal payments.</p>

				<h3>What happens after my trial?</h3>
				<p>You can upgrade to any plan any time during your trial. However, if you hit the end of your trial without choosing a plan, your account will be put on hold (your data will all stay safe for 30 days) until you choose a plan (or cancel).</p>

				<h3>Do I have to sign a long-term contract?</h3>
				<p>No. Monthly plans are just pay as you go, and you can cancel any time without penalty.</p>
			</div>
			<div class="pull-right">
				<h3>Can I change my plan at any time?</h3>
				<p>Yep. Simply visit the “Billing” page from the Settings menu and you’ll see your options.</p>

				<h3>Can I get a refund?</h3>
				<p>If you decide to cancel your account, you won't be charged again, but you are responsible for whatever charges have already been incurred for the current billing period.</p>
			</div>
		</div>
	</div>

@stop
