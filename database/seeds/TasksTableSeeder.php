<?php

use Illuminate\Database\Seeder;
use App\Task;
use App\TaskComment;
use App\TaskCategory;
use App\TaskPhase;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		DB::table('tasks')->delete();
		DB::unprepared('ALTER TABLE tasks AUTO_INCREMENT = 1;');
		Task::create(array('project_id' => 1, 'milestone_id' => 1, 'name' => 'Task 1', 'description' => 'Description of task 1', 'created_by' => 1, 'category_id' => 1, 'phase_id' => 1, 'assigned_to' => 1, 'due_date' => '2016-01-01 00:00:00'));
		Task::create(array('project_id' => 1, 'milestone_id' => 1, 'name' => 'Task 2', 'description' => 'Description of task 2', 'created_by' => 1));
		Task::create(array('project_id' => 1, 'milestone_id' => 1, 'name' => 'Task 3', 'description' => 'Description of task 3', 'created_by' => 1));
		Task::create(array('project_id' => 1, 'milestone_id' => 2, 'name' => 'A task with a long description', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ultricies gravida. Aliquam faucibus lorem vulputate congue euismod. Etiam laoreet condimentum augue, eget molestie nisl varius quis. Nullam et urna orci. Donec eget aliquet metus, pellentesque porta velit. Vestibulum vitae augue lorem. Donec ut ligula enim. Ut tincidunt, augue id egestas egestas, sem eros lacinia mauris, non euismod sapien mi vitae urna. Nam in varius ligula. Quisque hendrerit quis massa ut laoreet. Proin sit amet magna porta, laoreet lacus et, convallis sem. Mauris at mi id ante convallis tempor quis quis dui. Nullam accumsan erat scelerisque molestie lacinia. Sed pellentesque faucibus metus eu aliquam. Donec adipiscing odio at neque iaculis auctor.', 'created_by' => 1));
		Task::create(array('project_id' => 1, 'milestone_id' => 1, 'name' => 'Completed Task', 'description' => '', 'created_by' => 1, 'completed' => true));

		DB::table('task_comments')->delete();
		DB::unprepared('ALTER TABLE task_comments AUTO_INCREMENT = 1;');
		TaskComment::create(array('task_id' => 1, 'user_id' => 1, 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ultricies gravida. Aliquam faucibus lorem vulputate congue euismod. Etiam laoreet condimentum augue, eget molestie nisl varius quis. Nullam et urna orci. Donec eget aliquet metus, pellentesque porta velit. Vestibulum vitae augue lorem. Donec ut ligula enim. Ut tincidunt, augue id egestas egestas, sem eros lacinia mauris, non euismod sapien mi vitae urna. Nam in varius ligula. Quisque hendrerit quis massa ut laoreet. Proin sit amet magna porta, laoreet lacus et, convallis sem. Mauris at mi id ante convallis tempor quis quis dui. Nullam accumsan erat scelerisque molestie lacinia. Sed pellentesque faucibus metus eu aliquam. Donec adipiscing odio at neque iaculis auctor.'));
		TaskComment::create(array('task_id' => 1, 'user_id' => 1, 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ultricies gravida. Aliquam faucibus lorem vulputate congue euismod. Etiam laoreet condimentum augue, eget molestie nisl varius quis. Nullam et urna orci. Donec eget aliquet metus, pellentesque porta velit. Vestibulum vitae augue lorem. Donec ut ligula enim. Ut tincidunt, augue id egestas egestas, sem eros lacinia mauris, non euismod sapien mi vitae urna. Nam in varius ligula. Quisque hendrerit quis massa ut laoreet. Proin sit amet magna porta, laoreet lacus et, convallis sem. Mauris at mi id ante convallis tempor quis quis dui. Nullam accumsan erat scelerisque molestie lacinia. Sed pellentesque faucibus metus eu aliquam. Donec adipiscing odio at neque iaculis auctor.'));
		TaskComment::create(array('task_id' => 1, 'user_id' => 1, 'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ultricies gravida. Aliquam faucibus lorem vulputate congue euismod. Etiam laoreet condimentum augue, eget molestie nisl varius quis. Nullam et urna orci. Donec eget aliquet metus, pellentesque porta velit. Vestibulum vitae augue lorem. Donec ut ligula enim. Ut tincidunt, augue id egestas egestas, sem eros lacinia mauris, non euismod sapien mi vitae urna. Nam in varius ligula. Quisque hendrerit quis massa ut laoreet. Proin sit amet magna porta, laoreet lacus et, convallis sem. Mauris at mi id ante convallis tempor quis quis dui. Nullam accumsan erat scelerisque molestie lacinia. Sed pellentesque faucibus metus eu aliquam. Donec adipiscing odio at neque iaculis auctor.'));

		DB::table('task_phases')->delete();
		DB::unprepared('ALTER TABLE task_phases AUTO_INCREMENT = 1;');
		TaskPhase::create(array('project_id' => 1, 'name' => 'Design', 'description' => ''));
		TaskPhase::create(array('project_id' => 1, 'name' => 'Development', 'description' => ''));
		TaskPhase::create(array('project_id' => 1, 'name' => 'Testing', 'description' => ''));

		DB::table('task_categories')->delete();
		DB::unprepared('ALTER TABLE task_categories AUTO_INCREMENT = 1;');
		TaskCategory::create(array('project_id' => 1, 'name' => 'Bug', 'description' => 'Bugs are known problems with your code that need fixed.'));
		TaskCategory::create(array('project_id' => 1, 'name' => 'Issue', 'description' => 'Issues are potential problems that need investigated or researched.'));
		TaskCategory::create(array('project_id' => 1, 'name' => 'Feature', 'description' => 'Features are enhancements you want to add to your codebase.'));
	}
}
