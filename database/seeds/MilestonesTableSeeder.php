<?php

use Illuminate\Database\Seeder;
use App\Milestone;

class MilestonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('milestones')->delete();
        DB::unprepared('ALTER TABLE milestones AUTO_INCREMENT = 1;');
        Milestone::create(array('project_id' => 1, 'name' => 'Milestone 1', 'description' => 'Description of milestone 1'));
        Milestone::create(array('project_id' => 1, 'name' => 'A milestone with a long description', 'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ultricies gravida. Aliquam faucibus lorem vulputate congue euismod. Etiam laoreet condimentum augue, eget molestie nisl varius quis. Nullam et urna orci. Donec eget aliquet metus, pellentesque porta velit. Vestibulum vitae augue lorem. Donec ut ligula enim. Ut tincidunt, augue id egestas egestas, sem eros lacinia mauris, non euismod sapien mi vitae urna. Nam in varius ligula. Quisque hendrerit quis massa ut laoreet. Proin sit amet magna porta, laoreet lacus et, convallis sem. Mauris at mi id ante convallis tempor quis quis dui. Nullam accumsan erat scelerisque molestie lacinia. Sed pellentesque faucibus metus eu aliquam. Donec adipiscing odio at neque iaculis auctor.'));
        Milestone::create(array('project_id' => 1, 'name' => 'Milestone Empty', 'description' => 'Description of milestone 3'));
        Milestone::create(array('project_id' => 2, 'name' => 'Milestone 4', 'description' => 'Description of milestone 3'));
    }
}
