<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
		DB::table('projects')->delete();
		DB::unprepared('ALTER TABLE projects AUTO_INCREMENT = 1;');
		Project::create(array('owner' => 1, 'name' => 'Project 1', 'description' => 'Description of project 1'));
		Project::create(array('owner' => 1, 'name' => 'Project 2', 'description' => 'Description of project 2'));
		Project::create(array('owner' => 1, 'name' => 'Project 3', 'description' => 'Description of project 3'));

		DB::table('project_user')->delete();
		DB::unprepared('ALTER TABLE project_user AUTO_INCREMENT = 1;');
		DB::insert('INSERT INTO project_user (project_id, user_id) VALUES (?, ?)', array(1, 1));
		DB::insert('INSERT INTO project_user (project_id, user_id) VALUES (?, ?)', array(2, 1));
		DB::insert('INSERT INTO project_user (project_id, user_id) VALUES (?, ?)', array(1, 2));
		DB::insert('INSERT INTO project_user (project_id, user_id) VALUES (?, ?)', array(3, 2));
	}
}
