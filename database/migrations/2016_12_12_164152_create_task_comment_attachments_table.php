<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskCommentAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_comment_attachments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('comment_id')->nullable();
            $table->string('name')->nullable();
            $table->string('filename')->nullable();
            $table->integer('size')->nullable();
            $table->string('type')->nullable();
            $table->integer('uploaded_by')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_comment_attachments');
    }
}
